package com.ruoyi.web.controller.common;


import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysFileService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

@RestController
@RequestMapping("/public")
public class PublicController extends BaseController {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysFileService fileManagementService;

    @Autowired
    private ISysConfigService configService;
    @Autowired
    private ISysUserService userService;

    @Autowired

    private SysPermissionService sysPermissionService;

    /**
     * 使用<a>标签<a/>携带cookies访问文件
     * 必须具备 tool:file:view 权限
     * <p>
     * 预览文件
     */
    @GetMapping("/previewVideo")
    public void previewVideo(@RequestParam String path, @RequestParam(value = "token", defaultValue = "") String token, HttpServletResponse response, HttpServletRequest request) throws IOException {

        try {
            if (StringUtils.isEmpty(token)) {
                return;
            }
            String usernameFromToken = tokenService.getUsernameFromToken(token);
            if (usernameFromToken == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

            SysUser sysUser = userService.selectUserByUserName(usernameFromToken);
            if (sysUser == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

            Set<String> menuPermission = sysPermissionService.getMenuPermission(sysUser);
            if (menuPermission.contains("tool:file:view") || sysUser.isAdmin()) {
                FileUtils.play(path, request, response);
                return;
            }
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @GetMapping("/settings")
    public AjaxResult getPageSettings() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("config", JSON.parseObject(configService.decryptedConfig(), Object.class));
        return success(map);
    }
}
