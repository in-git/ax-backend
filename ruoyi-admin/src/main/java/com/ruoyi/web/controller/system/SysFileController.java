package com.ruoyi.web.controller.system;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.ISysFileService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static com.ruoyi.common.core.domain.AjaxResult.success;

@RestController
@RequestMapping("/tool/file")
public class SysFileController {
    @Autowired
    private ISysFileService fileManagementService;

    @PreAuthorize("@ss.hasPermi('tool:file:view')")
    @GetMapping("/path")
    public AjaxResult getPath(String path, Boolean onlyFolder) {
        return success(fileManagementService.listFiles(path, onlyFolder));
    }

    @GetMapping("/viewFile")
    public ResponseEntity<byte[]> viewFile(@RequestParam String path) throws IOException {
        return fileManagementService.viewFile(path);
    }

    @PreAuthorize("@ss.hasPermi('tool:file:add')")
    @GetMapping("/createFile")
    public AjaxResult createFile(@RequestParam String path, @RequestParam String type) {
        return success(fileManagementService.createFile(path, type));
    }

    @PreAuthorize("@ss.hasPermi('tool:file:delete')")
    @DeleteMapping("/deleteFile")
    public AjaxResult deleteFile(@RequestBody String path) {
        return success(fileManagementService.deleteFile(path));
    }

    @Data
    public static class FileRenameRequest {
        private String newFileName;
        private String oldFileName;
    }

    @PreAuthorize("@ss.hasPermi('tool:file:rename')")
    @PutMapping("/renameFile")
    public AjaxResult rename(@RequestBody FileRenameRequest renameRequest) {
        return success(fileManagementService.renameFile(renameRequest.getOldFileName(), renameRequest.getNewFileName()));
    }


    @Data
    public static class CloneFile {
        private String targetPath;
        private String files;
    }

    @PreAuthorize("@ss.hasPermi('tool:file:clone')")
    @PostMapping("/cloneFile")
    public AjaxResult cloneFile(@RequestBody CloneFile cloneFile) {
        return success(fileManagementService.cloneFiles(cloneFile.getTargetPath(), cloneFile.getFiles()));
    }


    @PreAuthorize("@ss.hasPermi('tool:file:attr')")
    @GetMapping("/fileAttr")
    public AjaxResult fileAttr(@RequestParam String path) {
        return success(fileManagementService.fileInfo(path));
    }



    @Data
    static class SaveFileVo {
        String path;
        String content;
    }

    @PreAuthorize("@ss.hasPermi('tool:file:save')")
    @PostMapping("/saveFile")
    public AjaxResult saveFile(@RequestBody SaveFileVo fileVo) {
        return success(fileManagementService.saveFile(fileVo.getPath(), fileVo.getContent()));
    }


    @PreAuthorize("@ss.hasPermi('tool:file:download')")
    @GetMapping("/download")
    public ResponseEntity<byte[]> download(@RequestParam String path) {
        return fileManagementService.download(path);
    }

}
