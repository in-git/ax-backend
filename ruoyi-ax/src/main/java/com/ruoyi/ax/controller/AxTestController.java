package com.ruoyi.ax.controller;

import com.ruoyi.ax.domain.AxTest;
import com.ruoyi.ax.service.IAxTestService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.VModel;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 测试Controller
 * 
 * @author AX
 *
 */
@RestController
@Validated
@RequestMapping("/ax/test")
public class AxTestController extends BaseController
{
    @Autowired
    private IAxTestService axTestService;

    /**
     * 查询测试列表
     */
    @PreAuthorize("@ss.hasPermi('ax:test:list')")
    @GetMapping("/list")
    public TableDataInfo list(AxTest axTest)
    {
        startPage();
        List<AxTest> list = axTestService.selectAxTestList(axTest);
        return getDataTable(list);
    }

    /**
    * 查询所有测试列表
    */
    @PreAuthorize("@ss.hasPermi('ax:test:list')")
    @GetMapping("/loadAll")
    public TableDataInfo loadAll()
    {
    List<AxTest> list =  axTestService.loadAllAxTest();
        return getDataTable(list);
    }

    /**
     * 导出测试列表
     */
    @PreAuthorize("@ss.hasPermi('ax:test:export')")
    @Log(title = "测试", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AxTest axTest)
    {

        List<AxTest> list = axTestService.selectAxTestList(axTest);
        ExcelUtil<AxTest> util = new ExcelUtil<AxTest>(AxTest.class);
        util.exportExcel(response, list, "测试数据");
    }

    /**
     * 获取测试详细信息
     */
    @PreAuthorize("@ss.hasPermi('ax:test:query')")
    @GetMapping(value = "/{testId}")
    public AjaxResult getInfo(@PathVariable("testId") Long testId)
    {
        return success(axTestService.selectAxTestByTestId(testId));
    }

    /**
     * 新增测试
     */
    @PreAuthorize("@ss.hasPermi('ax:test:add')")
    @Log(title = "测试", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Valid AxTest axTest)
    {
        return toAjax(axTestService.insertAxTest(axTest));
    }

    /**
     * 修改测试
     */
    @PreAuthorize("@ss.hasPermi('ax:test:edit')")
    @Log(title = "测试", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AxTest axTest)
    {
        return toAjax(axTestService.updateAxTest(axTest));
    }

    /**
     * 删除测试
     */
    @PreAuthorize("@ss.hasPermi('ax:test:remove')")
    @Log(title = "测试", businessType = BusinessType.DELETE)
	@DeleteMapping("/{testIds}")
    public AjaxResult remove(@PathVariable Long[] testIds)
    {
        return toAjax(axTestService.deleteAxTestByTestIds(testIds));
    }

    /**
     * 批量添加 测试
     */
    @PostMapping("/batch")
    @PreAuthorize("@ss.hasPermi('ax:test:add')")
    public AjaxResult batchAddition(@RequestBody @Valid AxTest[] axTest) {
        return toAjax(axTestService.bulkInsert(axTest));
    }

    /**
     * 导入 测试 excel数据
     */
    @PostMapping("/importData")
    @PreAuthorize("@ss.hasPermi('ax:test:add')")
    public AjaxResult importTest(MultipartFile[] files) {
        return success(axTestService.importData(files));
    }
    /**
     * 更新 测试 中的某个属性
     */
    @PostMapping("/updateAttr")
    @PreAuthorize("@ss.hasPermi('ax:test:add')")
    public AjaxResult updateAttr(@RequestBody @Valid VModel vModel) {
    return toAjax(axTestService.updateAttr(vModel));
    }
}
