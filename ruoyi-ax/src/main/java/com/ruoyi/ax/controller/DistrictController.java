package com.ruoyi.ax.controller;

import com.ruoyi.ax.domain.District;
import com.ruoyi.ax.service.IDistrictService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.VModel;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 地区Controller
 * 
 * @author AX
 *
 */
@RestController
@Validated
@RequestMapping("/ax/district")
public class DistrictController extends BaseController
{
    @Autowired
    private IDistrictService districtService;

    /**
     * 查询地区列表
     */
    @PreAuthorize("@ss.hasPermi('ax:district:list')")
    @GetMapping("/list")
    public TableDataInfo list(District district)
    {
        startPage();
        List<District> list = districtService.selectDistrictList(district);
        return getDataTable(list);
    }

    /**
    * 查询所有地区列表
    */
    @PreAuthorize("@ss.hasPermi('ax:district:list')")
    @GetMapping("/loadAll")
    public TableDataInfo loadAll()
    {
    List<District> list =  districtService.loadAllDistrict();
        return getDataTable(list);
    }

    /**
     * 导出地区列表
     */
    @PreAuthorize("@ss.hasPermi('ax:district:export')")
    @Log(title = "地区", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, District district)
    {
        startPage();
        List<District> list = districtService.selectDistrictList(district);
        ExcelUtil<District> util = new ExcelUtil<District>(District.class);
        util.exportExcel(response, list, "地区数据");
    }

    /**
     * 获取地区详细信息
     */
    @PreAuthorize("@ss.hasPermi('ax:district:query')")
    @GetMapping(value = "/{districtId}")
    public AjaxResult getInfo(@PathVariable("districtId") String districtId)
    {
        return success(districtService.selectDistrictByDistrictId(districtId));
    }

    /**
     * 新增地区
     */
    @PreAuthorize("@ss.hasPermi('ax:district:add')")
    @Log(title = "地区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Valid District district)
    {
        return toAjax(districtService.insertDistrict(district));
    }

    /**
     * 修改地区
     */
    @PreAuthorize("@ss.hasPermi('ax:district:edit')")
    @Log(title = "地区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody District district)
    {
        return toAjax(districtService.updateDistrict(district));
    }

    /**
     * 删除地区
     */
    @PreAuthorize("@ss.hasPermi('ax:district:remove')")
    @Log(title = "地区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{districtIds}")
    public AjaxResult remove(@PathVariable String[] districtIds)
    {
        return toAjax(districtService.deleteDistrictByDistrictIds(districtIds));
    }

    /**
     * 批量添加 地区
     */
    @PostMapping("/batch")
    @PreAuthorize("@ss.hasPermi('ax:district:add')")
    public AjaxResult batchAddition(@RequestBody @Valid District[] district) {
        return toAjax(districtService.bulkInsert(district));
    }

    /**
     * 导入 地区 excel数据
     */
    @PostMapping("/importData")
    @PreAuthorize("@ss.hasPermi('ax:district:add')")
    public AjaxResult importDistrict(MultipartFile[] files) {
        return success(districtService.importData(files));
    }
    /**
     * 更新 地区 中的某个属性
     */
    @PostMapping("/updateAttr")
    @PreAuthorize("@ss.hasPermi('ax:district:add')")
    public AjaxResult updateAttr(@RequestBody @Valid VModel vModel) {
    return toAjax(districtService.updateAttr(vModel));
    }
}
