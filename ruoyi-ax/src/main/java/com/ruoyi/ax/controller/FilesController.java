package com.ruoyi.ax.controller;

import com.ruoyi.ax.domain.Files;
import com.ruoyi.ax.service.IFilesService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 文件管理Controller
 *
 * @author AX
 * @date ${datetime}
 */
@RestController
@RequestMapping("/ax/files")
public class FilesController extends BaseController {
    @Autowired
    private IFilesService filesService;

    /**
     * 查询文件管理列表
     */
    @PreAuthorize("@ss.hasPermi('ax:files:list')")
    @GetMapping("/list")
    public TableDataInfo list(Files files) {
        startPage();
        List<Files> list = filesService.selectFilesList(files);
        return getDataTable(list);
    }

    /**
     * 查询所有文件管理列表
     */
    @PreAuthorize("@ss.hasPermi('ax:files:list')")
    @GetMapping("/loadAll")
    public TableDataInfo loadAll() {
        List<Files> list = filesService.loadAllFiles();
        return getDataTable(list);
    }

    /**
     * 导出文件管理列表
     */
    @PreAuthorize("@ss.hasPermi('ax:files:export')")
    @Log(title = "文件管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Files files) {
        List<Files> list = filesService.selectFilesList(files);
        ExcelUtil<Files> util = new ExcelUtil<Files>(Files.class);
        util.exportExcel(response, list, "文件管理数据");
    }

    /**
     * 获取文件管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('ax:files:query')")
    @GetMapping(value = "/{fileId}")
    public AjaxResult getInfo(@PathVariable("fileId") Long fileId) {
        return success(filesService.selectFilesByFileId(fileId));
    }

    /**
     * 新增文件管理
     */
    @PreAuthorize("@ss.hasPermi('ax:files:add')")
    @Log(title = "文件管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Files files) {
        return toAjax(filesService.insertFiles(files));
    }

    /**
     * 修改文件管理
     */
    @PreAuthorize("@ss.hasPermi('ax:files:edit')")
    @Log(title = "文件管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Files files) {
        return toAjax(filesService.updateFiles(files));
    }

    /**
     * 删除文件管理
     */
    @PreAuthorize("@ss.hasPermi('ax:files:remove')")
    @Log(title = "文件管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{fileIds}")
    public AjaxResult remove(@PathVariable Long[] fileIds) {
        return toAjax(filesService.deleteFilesByFileIds(fileIds));
    }

    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<Files> util = new ExcelUtil<Files>(Files.class);
        util.importTemplateExcel(response, "文件管理");
    }

    /**
     * 批量新增
     */
    @PreAuthorize("@ss.hasPermi('ax:files:add')")
    @Log(title = "文件管理", businessType = BusinessType.INSERT)
    @PostMapping("/batchAddition")
    public AjaxResult batchAdd(@RequestBody Files files[]) {
        return toAjax(filesService.batchAddition(files));
    }

}
