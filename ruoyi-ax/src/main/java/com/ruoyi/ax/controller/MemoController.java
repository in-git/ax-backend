package com.ruoyi.ax.controller;

import com.ruoyi.ax.domain.Memo;
import com.ruoyi.ax.service.IMemoService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.VModel;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 备忘录Controller
 * 
 * @author AX
 *
 */
@RestController
@RequestMapping("/ax/memo")
@Validated
public class MemoController extends BaseController
{
    @Autowired
    private IMemoService memoService;

    /**
     * 查询备忘录列表
     */
    @PreAuthorize("@ss.hasPermi('ax:memo:list')")
    @GetMapping("/list")
    public TableDataInfo list(Memo memo)
    {
        startPage();
        List<Memo> list = memoService.selectMemoList(memo);
        return getDataTable(list);
    }

    /**
    * 查询所有备忘录列表
    */
    @PreAuthorize("@ss.hasPermi('ax:ax:memo:list')")
    @GetMapping("/loadAll")
    public TableDataInfo loadAll()
    {
    List<Memo> list =  memoService.loadAllMemo();
        return getDataTable(list);
    }

    /**
     * 导出备忘录列表
     */
    @PreAuthorize("@ss.hasPermi('ax:memo:export')")
    @Log(title = "备忘录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Memo memo)
    {
        List<Memo> list = memoService.selectMemoList(memo);
        ExcelUtil<Memo> util = new ExcelUtil<Memo>(Memo.class);
        util.exportExcel(response, list, "备忘录数据");
    }

    /**
     * 获取备忘录详细信息
     */
    @PreAuthorize("@ss.hasPermi('ax:memo:query')")
    @GetMapping(value = "/{memoId}")
    public AjaxResult getInfo(@PathVariable("memoId") @Valid Long memoId)
    {
        return success(memoService.selectMemoByMemoId(memoId));
    }

    /**
     * 新增备忘录
     */
    @PreAuthorize("@ss.hasPermi('ax:memo:add')")
    @Log(title = "备忘录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Valid Memo memo)
    {
        return toAjax(memoService.insertMemo(memo));
    }

    /**
     * 修改备忘录
     */
    @PreAuthorize("@ss.hasPermi('ax:memo:edit')")
    @Log(title = "备忘录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Memo memo)
    {
        return toAjax(memoService.updateMemo(memo));
    }

    /**
     * 删除备忘录
     */
    @PreAuthorize("@ss.hasPermi('ax:memo:remove')")
    @Log(title = "备忘录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{memoIds}")
    public AjaxResult remove(@PathVariable Long[] memoIds)
    {
        return toAjax(memoService.deleteMemoByMemoIds(memoIds));
    }

    /**
     * 批量添加 备忘录
     */
    @PostMapping("/batch")
    @PreAuthorize("@ss.hasPermi('ax:memo:add')")
    public AjaxResult batchAddition(@RequestBody @Valid Memo[] memo) {
        return toAjax(memoService.bulkInsert(memo));
    }

    /**
     * 导入 备忘录 excel数据
     */
    @PostMapping("/importData")
    @PreAuthorize("@ss.hasPermi('ax:memo:add')")
    public AjaxResult importMemo(MultipartFile[] files) {
        return success(memoService.importData(files));
    }
    /**
     * 更新 备忘录 中的某个属性
     */
    @PostMapping("/updateAttr")
    @PreAuthorize("@ss.hasPermi('ax:memo:edit')")
    public AjaxResult updateAttr(@RequestBody @Valid VModel vModel) {
    return toAjax(memoService.updateAttr(vModel));
    }
}
