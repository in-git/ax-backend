package com.ruoyi.ax.controller;

import com.ruoyi.ax.domain.UserAssets;
import com.ruoyi.ax.service.IUserAssetsService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户资源Controller
 *
 * @author AX
 */
@RestController
@RequestMapping("/ax/userAssets")
public class UserAssetsController extends BaseController {
    @Autowired
    private IUserAssetsService userAssetsService;

    /**
     * 查询用户资源列表
     */
    @PreAuthorize("@ss.hasPermi('ax:userAssets:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserAssets userAssets) {
        startPage();
        List<UserAssets> list = userAssetsService.selectUserAssetsList(userAssets);
        return getDataTable(list);
    }


    /**
     * 获取用户主题
     */

    @Log(title = "用户资源", businessType = BusinessType.UPDATE)
    @GetMapping("/theme")
    public AjaxResult getUserTheme() {
        return success(userAssetsService.getUserTheme());
    }

    /**
     * 设置或更新用户主题
     */

    @Log(title = "用户资源", businessType = BusinessType.UPDATE)
    @PostMapping("/theme")
    public AjaxResult setUserTheme(@RequestBody String data) {
        return success(userAssetsService.setUserTheme(data));
    }
}
