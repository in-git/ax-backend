package com.ruoyi.ax.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
/**
 * 测试对象 ax_test
 * @author AX
 */

@Data
@TableName("ax_test")
public class AxTest
{
    private static final long serialVersionUID = 1L;

    /** 测试ID */
    @TableId
    private Long testId;
    /** 地区选择 */
    @Excel(name = "地区选择")
    @NotBlank(message = "地区选择不能为空")
    private String district;
    /** 图片选择 */
    @Excel(name = "图片选择")
    @NotNull(message = "图片选择不能为空")
    private String imageField;
    /** Markdown文件 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Markdown文件", width = 30, dateFormat = "yyyy-MM-dd")
    private Date mdField;
    /** 卡片封面 */
    @Excel(name = "卡片封面")
    private String cardCover;
    /** 备注信息 */
    @Excel(name = "备注信息")
    private String remark;
    /** 三向绑定 */
    @Excel(name = "三向绑定")
    private String model;
    /** create_time */
    private Date createTime;
    /** update_time */
    private Date updateTime;
}
