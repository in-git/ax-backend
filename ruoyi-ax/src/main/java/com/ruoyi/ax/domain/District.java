package com.ruoyi.ax.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
/**
 * 地区对象 ax_district
 * @author AX
 */

@Data
@TableName("ax_district")
public class District
{
    private static final long serialVersionUID = 1L;

    /** 自增id */
    @TableId
    private String districtId;

    /** 父及关系 */
    @Excel(name = "父及关系")
    @NotNull(message = "父及关系不能为空")
    private Integer pid;

    /** 地区名称 */
    @Excel(name = "地区名称")
    @NotBlank(message = "地区名称不能为空")
    private String district;

    /** 子属关系 */
    @Excel(name = "子属关系")
    @NotNull(message = "子属关系不能为空")
    private Integer level;
}
