package com.ruoyi.ax.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;
/**
 * 文件管理对象 ax_files
 * @author AX
 */

@Data
@TableName("ax_files")
public class Files
{
    private static final long serialVersionUID = 1L;

    /** 文件唯一标识 */
    @TableId
    private Long fileId;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String fileName;

    /** 文件存储路径 */
    @Excel(name = "文件存储路径")
    private String filePath;

    /** 文件大小（字节） */
    @Excel(name = "文件大小", readConverterExp = "字=节")
    private String fileSize;

    /** 文件类型 */
    @Excel(name = "文件类型")
    private String fileType;

    /** 文件描述 */
    @Excel(name = "文件描述")
    private String fileDescription;

    /** 记录创建时间 */
    private Date createTime;

    /** 记录最后更新时间 */
    private Date updateTime;

    /** 用户ID */
    private Long userId;

    /** 审核状态 */
    private Long status;

    /** file_storage_location */
    private String fileStorageLocation;

    /** external_link */
    private String externalLink;


}
