package com.ruoyi.ax.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;
/**
 * 备忘录对象 ax_memo
 * @author AX
 */

@Data
@TableName("ax_memo")
public class Memo
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId
    private Long memoId;

    @NotBlank(message = "标题不能为空")
    /** 备忘录标题 */
    @Excel(name = "备忘录标题")
    private String title;

    /** 备忘录的值 */
    @Excel(name = "备忘录的值")
    private String value;

    /** 备忘录描述 */
    @Excel(name = "备忘录描述")
    private String description;

    /** 附加值 */
    @Excel(name = "附加值")
    private String extra;

    /** 用户ID */
    private Long userId;

    /** 1：备忘录，2：记事本 */
    private String type;

    /** 部门ID */
    private Long deptId;

    /** 创建时间 */
    private Date createTime;

    /** 更新时间 */
    private Date updateTime;

}
