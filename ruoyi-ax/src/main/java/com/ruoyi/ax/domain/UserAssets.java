package com.ruoyi.ax.domain;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;
/**
 * 用户资源对象 ax_user_assets
 * @author AX
 */

@Data
@TableName("ax_user_assets")
public class UserAssets
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** user_id */
    @Excel(name = "user_id")
    private Long userId;

    /** menu_id */
    @Excel(name = "menu_id")
    private Long menuId;

    /** theme_data */
    private String themeData;

    /** create_time */
    private Date createTime;

    /** update_time */
    private Date updateTime;

    /** remark */
    private String remark;


}
