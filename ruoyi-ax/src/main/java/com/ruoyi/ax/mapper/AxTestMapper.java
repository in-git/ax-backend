package com.ruoyi.ax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.ax.domain.AxTest;
import org.apache.ibatis.annotations.Mapper;
/**
 * 测试Mapper接口
 * 
 * @author AX
 * @date ${datetime}
 */
@Mapper

public interface AxTestMapper   extends BaseMapper<AxTest> {}