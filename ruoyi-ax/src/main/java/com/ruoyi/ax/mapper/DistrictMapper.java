package com.ruoyi.ax.mapper;

import java.util.List;

import com.ruoyi.ax.domain.District;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 地区Mapper接口
 * 
 * @author AX
 * @date ${datetime}
 */
@Mapper

public interface DistrictMapper   extends BaseMapper<District> {}