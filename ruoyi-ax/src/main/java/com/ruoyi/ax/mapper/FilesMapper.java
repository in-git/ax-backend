package com.ruoyi.ax.mapper;

import java.util.List;

import com.ruoyi.ax.domain.Files;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 文件管理Mapper接口
 * 
 * @author AX
 * @date ${datetime}
 */
@Mapper

public interface FilesMapper   extends BaseMapper<Files> {}