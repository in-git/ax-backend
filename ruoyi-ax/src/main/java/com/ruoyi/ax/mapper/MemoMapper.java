package com.ruoyi.ax.mapper;

import java.util.List;

import com.ruoyi.ax.domain.Memo;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 备忘录Mapper接口
 * 
 * @author AX
 * @date ${datetime}
 */
@Mapper

public interface MemoMapper   extends BaseMapper<Memo> {}