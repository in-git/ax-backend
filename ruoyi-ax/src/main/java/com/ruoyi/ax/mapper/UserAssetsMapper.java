package com.ruoyi.ax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.ax.domain.UserAssets;
import org.apache.ibatis.annotations.Mapper;
/**
 * 用户资源Mapper接口
 * 
 * @author AX
 * @date ${datetime}
 */
@Mapper

public interface UserAssetsMapper   extends BaseMapper<UserAssets> {}