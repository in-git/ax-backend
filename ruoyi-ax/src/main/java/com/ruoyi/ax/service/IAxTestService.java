package com.ruoyi.ax.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ax.domain.AxTest;
import com.ruoyi.common.core.domain.VModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
/**
 * 测试Service接口
 *
 * @author AX
 * @date ${datetime}
 */
public interface IAxTestService extends IService<AxTest>
{
    /**
     * 查询测试
     *
     * @param testId 测试主键
     * @return 测试
     */
    public AxTest selectAxTestByTestId(Long testId);

    /**
     * 查询测试列表
     *
     * @param axTest 测试
     * @return 测试集合
     */
    public List<AxTest> selectAxTestList(AxTest axTest);

    /**
     * 新增测试
     *
     * @param axTest 测试
     * @return 结果
     */
    public int insertAxTest(AxTest axTest);

    /**
     * 修改测试
     *
     * @param axTest 测试
     * @return 结果
     */
    public int updateAxTest(AxTest axTest);

    /**
     * 批量删除测试
     *
     * @param testIds 需要删除的测试主键集合
     * @return 结果
     */
    public int deleteAxTestByTestIds(Long[] testIds);

    /**
    * 加载所有测试
    *
    * @return 结果
    */
    public List<AxTest> loadAllAxTest();

    /**
     * 创建多条测试
     *
     * @return 结果
     */
    public int bulkInsert(AxTest[] axTestList);


    /**
     * 创建多条测试
     *
     * @return 结果
    */
    public int importData(MultipartFile[] files);

    /**
     * 更新属性
     *
     * @return 结果
     */
    public int updateAttr(VModel vModel);
}
