package com.ruoyi.ax.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.ruoyi.ax.domain.District;
import com.ruoyi.common.core.domain.VModel;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.core.domain.VModel;
/**
 * 地区Service接口
 *
 * @author AX
 * @date ${datetime}
 */
public interface IDistrictService extends IService<District>
{
    /**
     * 查询地区
     *
     * @param districtId 地区主键
     * @return 地区
     */
    public District selectDistrictByDistrictId(String districtId);

    /**
     * 查询地区列表
     *
     * @param district 地区
     * @return 地区集合
     */
    public List<District> selectDistrictList(District district);

    /**
     * 新增地区
     *
     * @param district 地区
     * @return 结果
     */
    public int insertDistrict(District district);

    /**
     * 修改地区
     *
     * @param district 地区
     * @return 结果
     */
    public int updateDistrict(District district);

    /**
     * 批量删除地区
     *
     * @param districtIds 需要删除的地区主键集合
     * @return 结果
     */
    public int deleteDistrictByDistrictIds(String[] districtIds);

    /**
    * 加载所有地区
    *
    * @return 结果
    */
    public List<District> loadAllDistrict();

    /**
     * 创建多条地区
     *
     * @return 结果
     */
    public int bulkInsert(District[] districtList);


    /**
     * 创建多条地区
     *
     * @return 结果
    */
    public int importData(MultipartFile[] files);

    /**
     * 更新属性
     *
     * @return 结果
     */
    public int updateAttr(VModel vModel);
}
