package com.ruoyi.ax.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ax.domain.Files;

import java.util.List;

/**
 * 文件管理Service接口
 * 
 * @author AX
 * @date ${datetime}
 */
public interface IFilesService extends IService<Files>
{
    /**
     * 查询文件管理
     * 
     * @param fileId 文件管理主键
     * @return 文件管理
     */
    public Files selectFilesByFileId(Long fileId);

    /**
     * 查询文件管理列表
     * 
     * @param files 文件管理
     * @return 文件管理集合
     */
    public List<Files> selectFilesList(Files files);

    /**
     * 新增文件管理
     * 
     * @param files 文件管理
     * @return 结果
     */
    public int insertFiles(Files files);
    public int batchAddition(Files files[]);
    /**
     * 修改文件管理
     * 
     * @param files 文件管理
     * @return 结果
     */
    public int updateFiles(Files files);

    /**
     * 批量删除文件管理
     * 
     * @param fileIds 需要删除的文件管理主键集合
     * @return 结果
     */
    public int deleteFilesByFileIds(Long[] fileIds);

    /**
    * 加载所有文件管理
    *
    * @return 结果
    */
    public List<Files> loadAllFiles();
}
