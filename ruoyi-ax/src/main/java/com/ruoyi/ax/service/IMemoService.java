package com.ruoyi.ax.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.ruoyi.ax.domain.Memo;
import com.ruoyi.common.core.domain.VModel;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.core.domain.VModel;
/**
 * 备忘录Service接口
 *
 * @author AX
 * @date ${datetime}
 */
public interface IMemoService extends IService<Memo>
{
    /**
     * 查询备忘录
     *
     * @param memoId 备忘录主键
     * @return 备忘录
     */
    public Memo selectMemoByMemoId(Long memoId);

    /**
     * 查询备忘录列表
     *
     * @param memo 备忘录
     * @return 备忘录集合
     */
    public List<Memo> selectMemoList(Memo memo);

    /**
     * 新增备忘录
     *
     * @param memo 备忘录
     * @return 结果
     */
    public int insertMemo(Memo memo);

    /**
     * 修改备忘录
     *
     * @param memo 备忘录
     * @return 结果
     */
    public int updateMemo(Memo memo);

    /**
     * 批量删除备忘录
     *
     * @param memoIds 需要删除的备忘录主键集合
     * @return 结果
     */
    public int deleteMemoByMemoIds(Long[] memoIds);

    /**
    * 加载所有备忘录
    *
    * @return 结果
    */
    public List<Memo> loadAllMemo();

    /**
     * 创建多条备忘录
     *
     * @return 结果
     */
    public int bulkInsert(Memo[] memoList);


    /**
     * 创建多条备忘录
     *
     * @return 结果
    */
    public int importData(MultipartFile[] files);

    /**
     * 更新属性
     *
     * @return 结果
     */
    public int updateAttr(VModel vModel);
}
