package com.ruoyi.ax.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ax.domain.UserAssets;
import com.ruoyi.common.core.domain.VModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
/**
 * 用户资源Service接口
 *
 * @author AX
 * @date ${datetime}
 */
public interface IUserAssetsService extends IService<UserAssets>
{
    /**
     * 查询用户资源
     *
     * @param id 用户资源主键
     * @return 用户资源
     */
    public UserAssets selectUserAssetsById(Long id);

    /**
     * 查询用户资源列表
     *
     * @param userAssets 用户资源
     * @return 用户资源集合
     */
    public List<UserAssets> selectUserAssetsList(UserAssets userAssets);

    /**
     * 新增用户资源
     *
     * @param userAssets 用户资源
     * @return 结果
     */
    public int insertUserAssets(UserAssets userAssets);

    /**
     * 修改用户资源
     *
     * @param userAssets 用户资源
     * @return 结果
     */
    public int updateUserAssets(UserAssets userAssets);

    /**
     * 批量删除用户资源
     *
     * @param ids 需要删除的用户资源主键集合
     * @return 结果
     */
    public int deleteUserAssetsByIds(Long[] ids);

    /**
    * 加载所有用户资源
    *
    * @return 结果
    */
    public List<UserAssets> loadAllUserAssets();

    /**
     * 创建多条用户资源
     *
     * @return 结果
     */
    public int bulkInsert(UserAssets[] userAssetsList);


    /**
     * 创建多条用户资源
     *
     * @return 结果
    */
    public int importData(MultipartFile[] files);

    /**
     * 更新属性
     *
     * @return 结果
     */
    public int updateAttr(VModel vModel);

    public Object getUserTheme();

    int setUserTheme(String data);
}
