package com.ruoyi.ax.service.impl;

import com.ruoyi.ax.mapper.AxTestMapper;
import com.ruoyi.ax.domain.AxTest;
import com.ruoyi.ax.service.IAxTestService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.VModel;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 测试Service业务层处理
 *
 * @author AX
 * @date ${datetime}
 */
@Service
public class AxTestServiceImpl extends ServiceImpl<AxTestMapper, AxTest> implements IAxTestService {
    @Autowired
    private AxTestMapper axTestMapper;

    /**
     * 查询测试
     *
     * @param testId 测试主键
     * @return 测试
     */
    @Override
    public AxTest selectAxTestByTestId(Long testId) {
        return axTestMapper.selectById(testId);
    }

    /**
     * 查询测试列表
     *
     * @param axTest 测试
     * @return 测试
     */
    @Override
    public List<AxTest> selectAxTestList(AxTest axTest) {
        QueryWrapper<AxTest> wrapper = new QueryWrapper<AxTest>();
        wrapper.like(StringUtils.isNotEmpty(axTest.getDistrict()), "district", axTest.getDistrict());
        return axTestMapper.selectList(wrapper);
    }

    /**
     * 新增测试
     *
     * @param axTest 测试
     * @return 结果
     */

    @Override
    public int insertAxTest(AxTest axTest) {
        axTest.setCreateTime(DateUtils.getNowDate());
        return axTestMapper.insert(axTest);
    }

    /**
     * 修改测试
     *
     * @param axTest 测试
     * @return 结果
     */
    @Override
    public int updateAxTest(AxTest axTest) {
        axTest.setUpdateTime(DateUtils.getNowDate());
        return axTestMapper.updateById(axTest);
    }

    /**
     * 批量删除测试
     *
     * @param testIdList 需要删除的测试主键
     * @return 结果
     */

    @Override
    public int deleteAxTestByTestIds(Long[] testIdList) {
        return axTestMapper.deleteBatchIds(Arrays.asList(testIdList));
    }

    /**
     * 加载所有测试
     *
     * @return 结果
     */
    @Override
    public List<AxTest> loadAllAxTest() {
        return axTestMapper.selectList(null);
    }

    /**
     * 创建多条数据
     *
     * @param axTest
     * @return 结果
     */
    @Override
    public int bulkInsert(AxTest[] axTest) {
        return saveBatch(Arrays.asList(axTest)) ? 1 : 0;
    }

    /**
     * 通过excel导入数据
     *
     * @param files
     * @return
     */
    @Override
    public int importData(MultipartFile[] files) {
        MultipartFile file = files[0];
        if (null == file) return 0;
        ExcelUtil<AxTest> util = new ExcelUtil<AxTest>(AxTest.class);
        try {
            List<AxTest> axTestList = util.importExcel(file.getInputStream());
            return saveBatch(axTestList) ? 1 : 0;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 更新属性，前端-后端-数据库三向绑定
     * 用于表格快速修改参数
     *
     * @param model
     * @return 结果
     */
    @Override
    public int updateAttr(VModel model) {
        AxTest obj = new AxTest();
        Optional<AxTest> optionalAxTest = Optional.of(obj);
        AxTest axTest = optionalAxTest.get();
        axTest.setTestId(Long.valueOf(model.getId()));
        try {
            Field field = AxTest.class.getDeclaredField(model.getFieldName());
            field.setAccessible(true);
            field.set(axTest, model.getModelValue());
            return axTestMapper.updateById(axTest);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException("无法找到字段: " + model.getFieldName(), e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("无法访问字段: " + model.getFieldName(), e);
        }
    }
}
