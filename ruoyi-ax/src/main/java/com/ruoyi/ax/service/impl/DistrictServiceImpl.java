package com.ruoyi.ax.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.ax.domain.District;
import com.ruoyi.ax.mapper.DistrictMapper;
import com.ruoyi.ax.service.IDistrictService;
import com.ruoyi.common.core.domain.VModel;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 地区Service业务层处理
 *
 * @author AX
 * @date ${datetime}
 */
@Service
public class DistrictServiceImpl extends ServiceImpl<DistrictMapper, District> implements IDistrictService {
    @Autowired
    private DistrictMapper districtMapper;

    /**
     * 查询地区
     *
     * @param districtId 地区主键
     * @return 地区
     */
    @Override
    public District selectDistrictByDistrictId(String districtId) {
        return districtMapper.selectById(districtId);
    }

    /**
     * 查询地区列表
     *
     * @param district 地区
     * @return 地区
     */
    @Override
    public List<District> selectDistrictList(District district) {
        QueryWrapper<District> wrapper = new QueryWrapper<District>();
        wrapper.like(StringUtils.isNotEmpty(district.getDistrict()), "district", district.getDistrict());
        return districtMapper.selectList(wrapper);
    }

    /**
     * 新增地区
     *
     * @param district 地区
     * @return 结果
     */

    @Override
    public int insertDistrict(District district) {
        return districtMapper.insert(district);
    }

    /**
     * 修改地区
     *
     * @param district 地区
     * @return 结果
     */
    @Override
    public int updateDistrict(District district) {
        return districtMapper.updateById(district);
    }

    /**
     * 批量删除地区
     *
     * @param districtIdList 需要删除的地区主键
     * @return 结果
     */

    @Override
    public int deleteDistrictByDistrictIds(String[] districtIdList) {
        return districtMapper.deleteBatchIds(Arrays.asList(districtIdList));
    }

    /**
     * 加载所有地区
     *
     * @return 结果
     */
    @Override
    public List<District> loadAllDistrict() {
        return districtMapper.selectList(null);
    }

    /**
     * 创建多条数据
     *
     * @param district
     * @return 结果
     */
    @Override
    public int bulkInsert(District[] district) {
        return saveBatch(Arrays.asList(district)) ? 1 : 0;
    }

    /**
     * 通过excel导入数据
     *
     * @param files
     * @return
     */
    @Override
    public int importData(MultipartFile[] files) {
        MultipartFile file = files[0];
        if (null == file) return 0;
        ExcelUtil<District> util = new ExcelUtil<District>(District.class);
        try {
            List<District> districtList = util.importExcel(file.getInputStream());
            return saveBatch(districtList) ? 1 : 0;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 更新属性，前端-后端-数据库三向绑定
     * 用于表格快速修改参数
     *
     * @param model
     * @return 结果
     */
    @Override
    public int updateAttr(VModel model) {
        District obj = new District();
        Optional<District> optionalDistrict = Optional.of(obj);
        District district = optionalDistrict.get();
        district.setDistrictId(model.getId());
        try {
            Field field = District.class.getDeclaredField(model.getFieldName());
            field.setAccessible(true);
            field.set(district, model.getModelValue());
            return districtMapper.updateById(district);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException("无法找到字段: " + model.getFieldName(), e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("无法访问字段: " + model.getFieldName(), e);
        }
    }
}
