package com.ruoyi.ax.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.ax.domain.Files;
import com.ruoyi.ax.mapper.FilesMapper;
import com.ruoyi.ax.service.IFilesService;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * 文件管理Service业务层处理
 *
 * @author AX
 * @date ${datetime}
 */
@Service
public class FilesServiceImpl extends ServiceImpl<FilesMapper, Files> implements IFilesService {
    @Autowired
    private FilesMapper filesMapper;
    @Value("${ruoyi.profile}")
    private String profile;
    /**
     * 查询文件管理
     *
     * @param fileId 文件管理主键
     * @return 文件管理
     */
    @Override
    public Files selectFilesByFileId(Long fileId) {
        return filesMapper.selectById(fileId);
    }

    /**
     * 查询文件管理列表
     *
     * @param files 文件管理
     * @return 文件管理
     */
    @Override
    public List<Files> selectFilesList(Files files) {
        QueryWrapper<Files> wrapper = new QueryWrapper<Files>();
        SysUser user = SecurityUtils.getLoginUser().getUser();
        wrapper.like(StringUtils.isNotEmpty(files.getFileName()), "file_name", files.getFileName());
        wrapper.like(StringUtils.isNotEmpty(files.getFileType()), "file_type", files.getFileType());
        wrapper.like(files.getUserId() != null, "user_id", user.getUserId());
        return filesMapper.selectList(wrapper);
    }

    /**
     * 新增文件管理
     *
     * @param files 文件管理
     * @return 结果
     */

    @Override
    public int insertFiles(Files files) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        files.setCreateTime(DateUtils.getNowDate());
        files.setUserId(user.getUserId());
        return filesMapper.insert(files);
    }

    /**
     * @param files
     * @return
     */
    @Override
    public int batchAddition(Files[] files) {
        int result = 0;
        for (Files file : files) {
            result += insertFiles(file);
        }
        return result;
    }

    /**
     * 修改文件管理
     *
     * @param files 文件管理
     * @return 结果
     */
    @Override
    public int updateFiles(Files files) {
        files.setUpdateTime(DateUtils.getNowDate());
        return filesMapper.updateById(files);
    }

    /**
     * 批量删除文件管理
     *
     * @param fileIdList 需要删除的文件管理主键
     * @return 结果
     */

    @Override
    public int deleteFilesByFileIds(Long[] fileIdList) {
        List<Files> filesList = filesMapper.selectBatchIds(Arrays.asList(fileIdList));
        for (Files f : filesList) {
            String location = f.getFileStorageLocation();
            File file = Paths.get(profile,location.replace("/profile","")).toFile();
            if (file.exists()) {
                file.delete();
            }

        }
        return filesMapper.deleteBatchIds(Arrays.asList(fileIdList));
    }

    /**
     * 加载所有文件管理
     *
     * @return 结果
     */
    @Override
    public List<Files> loadAllFiles() {
        return filesMapper.selectList(null);
    }
}
