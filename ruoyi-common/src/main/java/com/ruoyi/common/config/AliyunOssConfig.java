package com.ruoyi.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zouhuu
 * @description 阿里云对象存储 配置
 * @date 2022/06/16 14:08:21
 */
@Component
@Data
@ConfigurationProperties(prefix = "aliyunoss")
public class AliyunOssConfig {
    /**
     * 地域节点
     */
    private String endpoint;

    /**
     * AccessKey
     */
    private String accessKeyId;

    /**
     * AccessKey秘钥
     */
    private String accessKeySecret;

    /**
     * bucket名称
     */
    private String bucketName;

    /**
     * bucket下文件夹的路径
     */
    private String filehost;

    /**
     * 访问域名
     */
    private String url;


}

