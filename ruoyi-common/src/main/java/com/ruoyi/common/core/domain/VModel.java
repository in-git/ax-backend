package com.ruoyi.common.core.domain;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class VModel {
    /**
     * 目标字段
     */
    @NotBlank(message = "字段名不能为空")
    String fieldName;
    /**
     * 需要修改的值
     */

    String modelValue;
    /**
     * 根据ID修改
     */
    @NotBlank(message = "字段名不能为空")
    String  id;
}
