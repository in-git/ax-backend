package com.ruoyi.common.exception;

import lombok.Data;

/**
 * 演示模式异常
 * 
 * @author 吴文龙
 */
@Data
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
