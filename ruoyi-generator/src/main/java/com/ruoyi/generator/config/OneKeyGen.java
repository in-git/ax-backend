package com.ruoyi.generator.config;

import lombok.Data;

@Data
public class OneKeyGen {
    /*表单名称*/
    String tableName;
    /*类型*/
    String type;
    /*前端地址*/
    String frontendPath;
    /*后端地址*/
    String backendPath;
    /*是否立即执行SQL*/
    boolean runSql;
    /*是否强制覆盖文件*/
    boolean forceOverride;
}
