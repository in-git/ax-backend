package com.ruoyi.generator.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.generator.config.OneKeyGen;
import com.ruoyi.generator.domain.SinglePage;
import com.ruoyi.generator.service.IGenTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/tool/gen")
@Validated
public class CustomGenerate extends BaseController {
    /*
     * 生成代码且直接写入文件
     * */
    @Autowired
    private IGenTableService genTableService;

    @PreAuthorize("@ss.hasPermi('tool:gen:gen-code-file')")
    @PostMapping("/genCodeFile")
    public AjaxResult generatedCodeWriteToFile(@RequestBody OneKeyGen oneKeyGen) {
        return success(genTableService.generatedCodeWriteToFile(oneKeyGen));
    }

    @PreAuthorize("@ss.hasPermi('tool:gen:gen-code-file')")
    @PostMapping("/generateSinglePage")
    public AjaxResult generateSinglePage(@Valid @RequestBody SinglePage singlePage) {
        return success(genTableService.generateSinglePage(singlePage));
    }
}
