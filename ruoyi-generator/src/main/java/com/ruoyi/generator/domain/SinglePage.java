package com.ruoyi.generator.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SinglePage {
    @NotBlank
    /*菜单名称*/
    private String menuName;
    /*排序*/
    private int orderNum;
    /*路由*/
    private String path;
    /*组件路径*/
    @NotBlank
    private String component;
    /*菜单类型*/
    @NotBlank
    private String menuType;
    /*菜单ICON*/
    private String icon;
    /*文件绝对路径*/
    @NotBlank
    private String filePath;
    /*生成的类型*/
    private String type;
    /*父级ID*/
    private int parentId;
    /*是否创建文件*/
    private String fileCreated;
    /*自定义文件名*/
    private String  fileName;
}
