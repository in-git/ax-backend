package com.ruoyi.generator.domain;

import lombok.Data;

/*
* 这个类用于统一管理模版
* */
@Data
public class Templates {

    public static String[] apiTemplates = {
            "vm/js/api.ts.vm",
            "vm/js/types.ts.vm"
    };

    public  static  String[] sqlTemplate = {
            "vm/sql/sql.vm",
    };

    public  static  String[] javaTemplates = {
            "vm/java/domain.java.vm",
            "vm/java/mapper.java.vm",
            "vm/java/service.java.vm",
            "vm/java/serviceImpl.java.vm",
            "vm/java/controller.java.vm",
//            "vm/xml/mapper.xml.vm",
    };

    public  static  String[] pcTemplates = {
            /*数据*/
            "vm/ax/data/column.ts.vm",
            "vm/ax/data/card.ts.vm",
            "vm/ax/data/form.ts.vm",
            "vm/ax/data/form.message.ts.vm",
            "vm/ax/data/form.actions.ts.vm",
            "vm/ax/data/table.ts.vm",
            "vm/ax/data/curd.ts.vm",
            "vm/ax/data/options.ts.vm",
            /*页面*/
            "vm/ax/pages/AXForm.vue.vm",
            "vm/ax/pages/AXFooter.vue.vm",
            "vm/ax/pages/AXHead.vue.vm",
            "vm/ax/pages/AXCard.vue.vm",
            "vm/ax/pages/AXTable.vue.vm",
            "vm/ax/pages/index.vue.vm",
            "vm/ax/readme.md.vm"
    };
    public  static String[] mobileTemplates = {
            /*数据*/
            "vm/mobile/data/column.ts.vm",
            "vm/mobile/data/card.ts.vm",
            "vm/mobile/data/form.ts.vm",
            "vm/mobile/data/table.ts.vm",
            "vm/mobile/data/curd.ts.vm",
            "vm/mobile/data/options.ts.vm",
            /*页面*/
            "vm/mobile/index.vue.vm",
            "vm/mobile/view/Data.vue.vm",
            "vm/mobile/form/Form.vue.vm",
            "vm/mobile/view/Head.vue.vm",
            "vm/mobile/view/View.vue.vm",
            "vm/mobile/view/components/Sort.vue.vm",
            "vm/mobile/view/components/Setting.vue.vm",
            "vm/mobile/view/components/Search.vue.vm",
    };
}
