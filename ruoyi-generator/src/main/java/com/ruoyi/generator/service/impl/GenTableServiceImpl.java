package com.ruoyi.generator.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.GenConstants;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.generator.config.OneKeyGen;
import com.ruoyi.generator.domain.GenTable;
import com.ruoyi.generator.domain.GenTableColumn;
import com.ruoyi.generator.domain.SinglePage;
import com.ruoyi.generator.mapper.GenTableColumnMapper;
import com.ruoyi.generator.mapper.GenTableMapper;
import com.ruoyi.generator.service.IGenTableService;
import com.ruoyi.generator.util.GenUtils;
import com.ruoyi.generator.util.VelocityInitializer;
import com.ruoyi.generator.util.VelocityUtils;
import com.ruoyi.system.service.ISysMenuService;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 业务 服务层实现
 *
 * @author ruoyi
 */
@Service
public class GenTableServiceImpl implements IGenTableService {
    private static final Logger log = LoggerFactory.getLogger(GenTableServiceImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private GenTableMapper genTableMapper;

    @Autowired
    private GenTableColumnMapper genTableColumnMapper;

    @Autowired
    ISysMenuService iSysMenuService;
    /**
     * 查询业务信息
     *
     * @param id 业务ID
     * @return 业务信息
     */
    @Override
    public GenTable selectGenTableById(Long id) {
        GenTable genTable = genTableMapper.selectGenTableById(id);
        setTableFromOptions(genTable);
        return genTable;
    }

    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    @Override
    public List<GenTable> selectGenTableList(GenTable genTable) {
        return genTableMapper.selectGenTableList(genTable);
    }

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    @Override
    public List<GenTable> selectDbTableList(GenTable genTable) {
        return genTableMapper.selectDbTableList(genTable);
    }

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    @Override
    public List<GenTable> selectDbTableListByNames(String[] tableNames) {
        return genTableMapper.selectDbTableListByNames(tableNames);
    }

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    @Override
    public List<GenTable> selectGenTableAll() {
        return genTableMapper.selectGenTableAll();
    }

    /**
     * 修改业务
     *
     * @param genTable 业务信息
     * @return 结果
     */
    @Override
    @Transactional
    public void updateGenTable(GenTable genTable) {
        if (null == genTable.getOptions()) return;
        String options = JSON.toJSONString(genTable.getOptions());
        genTable.setOptions(options);
        int row = genTableMapper.updateGenTable(genTable);
        if (row > 0) {
            for (GenTableColumn cenTableColumn : genTable.getColumns()) {
                genTableColumnMapper.updateGenTableColumn(cenTableColumn);
            }
        }
    }

    /**
     * 删除业务对象
     *
     * @param tableIds 需要删除的数据ID
     * @return 结果
     */
    @Override
    @Transactional
    public void deleteGenTableByIds(Long[] tableIds) {
        genTableMapper.deleteGenTableByIds(tableIds);
        genTableColumnMapper.deleteGenTableColumnByIds(tableIds);
    }

    /**
     * 创建表
     *
     * @param sql 创建表语句
     * @return 结果
     */
    @Override
    public boolean createTable(String sql) {
        return genTableMapper.createTable(sql) == 0;
    }

    /**
     * 导入表结构
     *
     * @param tableList 导入表列表
     */
    @Override
    @Transactional
    public void importGenTable(List<GenTable> tableList, String operName) {
        try {
            for (GenTable table : tableList) {
                String tableName = table.getTableName();
                GenUtils.initTable(table, operName);
                int row = genTableMapper.insertGenTable(table);
                if (row > 0) {
                    // 保存列信息
                    List<GenTableColumn> genTableColumns = genTableColumnMapper.selectDbTableColumnsByName(tableName);
                    for (GenTableColumn column : genTableColumns) {
                        GenUtils.initColumnField(column, table);
                        genTableColumnMapper.insertGenTableColumn(column);
                    }
                }
            }
        } catch (Exception e) {
            throw new ServiceException("导入失败：" + e.getMessage());
        }
    }

    /**
     * 预览代码
     *
     * @param tableId 表编号
     * @return 预览数据列表
     */
    @Override
    public Map<String, String> previewCode(Long tableId, String type) {
        Map<String, String> dataMap = new LinkedHashMap<>();
        // 查询表信息
        GenTable table = genTableMapper.selectGenTableById(tableId);
        // 设置主键列信息
        setPkColumn(table);
        /*读取模板文件*/
        VelocityInitializer.initVelocity();
        /*设置模板变量信息*/
        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(type);

        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            // 合并模板和变量
            tpl.merge(context, sw);
            dataMap.put(template, sw.toString());
        }
        return dataMap;
    }

    /**
     * 生成代码（下载方式）
     *
     * @param tableName 表名称
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String tableName, String type) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        generatorCode(tableName, zip, type);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名称
     */
    @Override
    public void generatorCode(String tableName, String type) {
        // 查询表信息
        GenTable table = genTableMapper.selectGenTableByName(tableName);

        // 设置主键列信息
        setPkColumn(table);
        VelocityInitializer.initVelocity();
        //植入模板相关变量
        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(type);
        for (String template : templates) {
            if (!StringUtils.containsAny(template, "sql.vm", "api.ts.vm", "index.vue.vm")) {
                // 渲染模板
                StringWriter sw = new StringWriter();
                Template tpl = Velocity.getTemplate(template, Constants.UTF8);
                tpl.merge(context, sw);
            }
        }
    }

    /**
     * 同步数据库
     *
     * @param tableName 表名称
     */
    @Override
    @Transactional
    public void synchDb(String tableName) {
        GenTable table = genTableMapper.selectGenTableByName(tableName);
        List<GenTableColumn> tableColumns = table.getColumns();
        Map<String, GenTableColumn> tableColumnMap = tableColumns.stream().collect(Collectors.toMap(GenTableColumn::getColumnName, Function.identity()));

        List<GenTableColumn> dbTableColumns = genTableColumnMapper.selectDbTableColumnsByName(tableName);
        if (StringUtils.isEmpty(dbTableColumns)) {
            throw new ServiceException("同步数据失败，原表结构不存在");
        }
        List<String> dbTableColumnNames = dbTableColumns.stream().map(GenTableColumn::getColumnName).collect(Collectors.toList());

        dbTableColumns.forEach(column -> {
            GenUtils.initColumnField(column, table);
            if (tableColumnMap.containsKey(column.getColumnName())) {
                GenTableColumn prevColumn = tableColumnMap.get(column.getColumnName());
                column.setColumnId(prevColumn.getColumnId());
                if (column.isList()) {
                    // 如果是列表，继续保留查询方式/字典类型选项
                    column.setDictType(prevColumn.getDictType());
                    column.setQueryType(prevColumn.getQueryType());
                }
                if (StringUtils.isNotEmpty(prevColumn.getIsRequired()) && !column.isPk()
                        && (column.isInsert() || column.isEdit())
                        && ((column.isUsableColumn()) || (!column.isSuperColumn()))) {
                    // 如果是(新增/修改&非主键/非忽略及父属性)，继续保留必填/显示类型选项
                    column.setIsRequired(prevColumn.getIsRequired());
                    column.setHtmlType(prevColumn.getHtmlType());
                }
                genTableColumnMapper.updateGenTableColumn(column);
            } else {
                genTableColumnMapper.insertGenTableColumn(column);
            }
        });

        List<GenTableColumn> delColumns = tableColumns.stream().filter(column -> !dbTableColumnNames.contains(column.getColumnName())).collect(Collectors.toList());
        if (StringUtils.isNotEmpty(delColumns)) {
            genTableColumnMapper.deleteGenTableColumns(delColumns);
        }
    }

    /**
     * 批量生成代码（下载方式）
     *
     * @param tableNames 表数组
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String[] tableNames, String type) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames) {
            generatorCode(tableName, zip, type);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 查询表信息并生成代码
     */
    private void generatorCode(String tableName, ZipOutputStream zip, String type) {
        // 查询表信息
        GenTable table = genTableMapper.selectGenTableByName(tableName);

        // 设置主键列信息
        setPkColumn(table);
        VelocityInitializer.initVelocity();
        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(type);

        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            tpl.merge(context, sw);
            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(VelocityUtils.getFileName(template, table)));
                IOUtils.write(sw.toString(), zip, Constants.UTF8);
                IOUtils.closeQuietly(sw);
                zip.flush();
                zip.closeEntry();
            } catch (IOException e) {
                log.error("渲染模板失败，表名：" + table.getTableName(), e);
            }
        }
    }


    /**
     * 执行sql语句
     *
     * @param sqlScript Sql
     */
    @Override
    public int runSql(String sqlScript) {
        try {
            int success = 1;
            // Split the script by semicolon and execute each command separately
            for (String sql : sqlScript.split(";")) {
                if (!sql.trim().isEmpty()) {
                    jdbcTemplate.execute(sql);
                }
            }
            return success;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 生成代码且要写入文件
     *
     * @param oneKeyGen 一键生成代码
     * @return
     */
    @Override
    public Map<String, String> generatedCodeWriteToFile(OneKeyGen oneKeyGen) {
        Map<String, String> dataMap = new LinkedHashMap<>();
        GenTable table = genTableMapper.selectGenTableByName(oneKeyGen.getTableName());
        // 设置主键列信息
        setPkColumn(table);
        VelocityInitializer.initVelocity();
        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(oneKeyGen.getType());
        for (String template : templates) {
            // 渲染模板,生成的代码内容
            StringWriter sw = new StringWriter();

            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            tpl.merge(context, sw);
            String fileName = VelocityUtils.getFileName(template, table);
            Path fullPath = null;
            String backendPath = oneKeyGen.getBackendPath();
            String frontendPath = oneKeyGen.getFrontendPath();
            /*处理vue的代码*/
            if (fileName.startsWith("vue") && StringUtils.isNotEmpty(frontendPath)) {
                fullPath = Paths.get(oneKeyGen.getFrontendPath(), fileName.replace("vue/", "src/"));
            }
            /*处理java的代码*/
            else if (fileName.startsWith("main") && StringUtils.isNotEmpty(backendPath)) {
                fullPath = Paths.get(backendPath, "src/", fileName);
            } else if (fileName.contains("sql") && oneKeyGen.isRunSql()) {
                runSql(sw.toString());
                break;
            }
            if (fullPath != null) {
                createFileWithContent(String.valueOf(fullPath), sw.toString(), oneKeyGen.isForceOverride());
            }

        }
        return dataMap;
    }

    public String extractPath(String filePath) {
        // 找到 "src\\views\\modules\\" 后面的路径
        int startIndex = filePath.indexOf("src\\views\\modules\\");
        if (startIndex != -1) {
            return filePath.substring(startIndex + "src\\views\\modules\\".length());
        } else {
            return ""; // 如果找不到对应路径，可以返回空字符串或者 null，或者抛出异常，视情况而定
        }
    }
    /**
     * @param singlePage
     * 页面类型
     * 创建菜单/生成VUE文件
     * @return
     */
    @Override
    public int generateSinglePage(SinglePage singlePage) {

        final String IS_CREATE_FILE = "0";
        StringWriter sw = new StringWriter();
        //植入模板相关变量
        VelocityContext context = new VelocityContext();
        if (StringUtils.isEmpty(singlePage.getType())) {
            singlePage.setType("single");
        }
        Path filePath = Paths.get(singlePage.getFilePath(), "/src/views/modules", singlePage.getComponent().concat(".vue"));
        /*创建菜单*/
        SysMenu sysMenu = new SysMenu();
        sysMenu.setMenuName(singlePage.getMenuName());
        sysMenu.setIcon(singlePage.getIcon());
        sysMenu.setOrderNum(singlePage.getOrderNum());
        sysMenu.setComponent(singlePage.getComponent().replace(".vue", ""));
        sysMenu.setMenuType("C");
        sysMenu.setParentId((long) singlePage.getParentId());
        iSysMenuService.insertMenu(sysMenu);

        if (IS_CREATE_FILE.equals(singlePage.getFileCreated())) {
            /*创建文件*/
            VelocityInitializer.initVelocity();


            String templatePath = "vm/sfc/" + singlePage.getType() + ".vue.vm";
            Template template = Velocity.getTemplate(templatePath, Constants.UTF8);
            template.merge(context, sw);
            try {
                Path directoryPath = filePath.getParent();
                if (directoryPath != null) {
                    try {
                        Files.createDirectories(directoryPath);
                    } catch (IOException e) {
                        System.err.format("Failed to create directories: %s%n", e);
                        return 0;
                    }
                }
                //文件存在则放弃创建
                if (filePath.toFile().exists()) {
                    return 0;
                }
                Files.createFile(filePath);
                Files.write(filePath, sw.toString().getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return 1;
    }

    /**
     * @param filePath      文件路径
     * @param fileContent   文件内容
     * @param forceOverride 是否强制覆盖
     */
    public static void createFileWithContent(String filePath, String fileContent, boolean forceOverride) {

        try {

            Path path = Paths.get(filePath);
            if (!Files.exists(path.getParent())) {
                Files.createDirectories(path.getParent());
            }
            File file = path.toFile();
            if (!file.exists() || forceOverride) {
                Files.write(path, fileContent.getBytes());
            }

        } catch (IOException e) {
            System.err.println("Error creating file: " + e.getMessage());
        }

    }

    /**
     * 设置主键列信息
     *
     * @param table 业务表信息
     */
    public void setPkColumn(GenTable table) {
        for (GenTableColumn column : table.getColumns()) {
            if (column.isPk()) {
                table.setPkColumn(column);
                break;
            }
        }
        if (StringUtils.isNull(table.getPkColumn())) {
            table.setPkColumn(table.getColumns().get(0));
        }
    }


    /**
     * 设置代码生成其他选项值
     *
     * @param genTable 设置后的生成对象
     */
    public void setTableFromOptions(GenTable genTable) {
        if (null == genTable.getOptions()) {
            return;
        }
        JSONObject paramsObj = JSON.parseObject(genTable.getOptions().toString());
        if (StringUtils.isNotNull(paramsObj)) {
            String parentMenuId = paramsObj.getString(GenConstants.PARENT_MENU_ID);
            String parentMenuName = paramsObj.getString(GenConstants.PARENT_MENU_NAME);
            genTable.setParentMenuId(parentMenuId);
            genTable.setParentMenuName(parentMenuName);
        }
    }


}