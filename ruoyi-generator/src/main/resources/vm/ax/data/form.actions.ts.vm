/**
  ----------------------
  当前文件用于动态新增表单
  此处提供是的操作函数/方法
  控制 @file ./form.ts 中的属性
  ----------------------
 */
import type {  ${ModuleName}${BusinessName} } from '@/api/modules/${moduleName}/${businessName}/types';
import { nanoid } from 'nanoid';
import { ${businessName}FormConfig } from './form';

type Item = {
  data: ${ModuleName}${BusinessName};
  id: string;
};


/**
 * @description:
 *  动态表单，可以一次添加多条数据
 *  触发该方法后，可以新增一条数据
 *  改变 @param {current} 指向
 */
export const add${BusinessName}Form = () => {
  let id = nanoid();
  let data = {
    ...${businessName}FormConfig.value.raw,
  };
  ${businessName}FormConfig.value.list.push({
    data,
    id,
  });
  ${businessName}FormConfig.value.selectedId = id;
  ${businessName}FormConfig.value.current = data;
};


/**
 * @description: 点击选择一项
 * @param {Item} item
 */
export const select${BusinessName}Form = (item: Item) => {
  ${businessName}FormConfig.value.current = item.data;
  ${businessName}FormConfig.value.selectedId = item.id;
};
/**
 * @description:
 *  根据ID删除动态新增的一项
 * @param {string} targetId
 * @param {number} index
 */
export const del${BusinessName}Form = (targetId: string, index: number) => {
  let maxSize = ${businessName}FormConfig.value.list.length;
  if (maxSize === 1) {
    return;
  }
  ${businessName}FormConfig.value.list = ${businessName}FormConfig.value.list.filter(e => e.id !== targetId);
  if (${businessName}FormConfig.value.list.length === index) {
    setDataByIndex(index - 1);
  } else {
    setDataByIndex(index);
  }
};
/**
 * @description: 根据索引设置当前表单的值
 * @param {number} i
 */
const setDataByIndex = (i: number) => {
  const data = ${businessName}FormConfig.value.list[i];
  if (data) {
    ${businessName}FormConfig.value.selectedId = data.id;
    ${businessName}FormConfig.value.current = data.data;
  }
};
