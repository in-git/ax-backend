package com.ruoyi.system.domain.vo;

import lombok.Data;

@Data
public class CommandVo {
    String command;
    String root;
}