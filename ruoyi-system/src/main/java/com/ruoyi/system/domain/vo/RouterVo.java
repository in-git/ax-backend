package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * 路由配置信息
 *
 * @author ruoyi
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouterVo {
    /**
     * 路由名字
     */
    private String name;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现
     */
    private boolean hidden;

    /**
     * 组件地址
     */
    private String component;



    /**
     * 标题
     */
    private String title;

    /**
     * 标题
     */
    private String icon;

    /**
     * 子路由
     */
    private List<RouterVo> children;

    /**
     * 菜单类型
     */
    private String menuType;

    /**
     * 菜单ID
     */
    private Long id;

    /**窗口高度*/
    private Integer height;

    /**窗口宽度*/
    private Integer width;
}
