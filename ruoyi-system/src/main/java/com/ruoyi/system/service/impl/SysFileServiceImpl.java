package com.ruoyi.system.service.impl;

import cn.hutool.core.io.FileUtil;
import com.ruoyi.system.domain.vo.FileAttr;
import com.ruoyi.system.domain.vo.FileInfoVo;
import com.ruoyi.system.service.ISysFileService;
import lombok.SneakyThrows;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@Service
public class SysFileServiceImpl implements ISysFileService {

    public static boolean isImage(File file) {
        if (!file.exists() || file.isDirectory()) {
            return false;
        }

        // 获取文件的MIME类型
        try {
            Path path = Paths.get(file.getPath());
            String mimeType = Files.probeContentType(path);
            if (mimeType != null && mimeType.startsWith("image")) {
                return true; // 如果MIME类型以"image"开头，则视为图片
            }
        } catch (IOException e) {
            return false;
        }

        return false;
    }


    /**
     * 压缩且转成base64
     *
     * @param imagePath 图片路径
     */
    public static String convertImageToBase64(String imagePath) throws IOException {

        try {
            // 读取原始图片
            BufferedImage originalImage = ImageIO.read(new File(imagePath));
            if (originalImage == null) {
                return "";
            }
            // 创建一个新的带白色背景的图片
            BufferedImage newImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = newImage.createGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, newImage.getWidth(), newImage.getHeight());
            graphics.drawImage(originalImage, 0, 0, null);
            graphics.dispose();

            // 将图片转换为JPEG格式
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(newImage, "jpg", outputStream);

            // 将JPEG图片转换为Base64字符串
            byte[] imageBytes = outputStream.toByteArray();
            String base64String = Base64.getEncoder().encodeToString(imageBytes);
            // 将字节数组转换为Base64编码字符串
            return "data:image/jpg;base64," + base64String;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public static void sortFileInfoList(List<FileInfoVo> fileInfoList) {
        Collections.sort(fileInfoList, new Comparator<FileInfoVo>() {
            @Override
            public int compare(FileInfoVo fileInfo1, FileInfoVo fileInfo2) {
                // 首先比较isLeaf属性，isLeaf为false的排在前面
                if (!fileInfo1.getIsLeaf() && fileInfo2.getIsLeaf()) {
                    return -1;
                } else if (fileInfo1.getIsLeaf() && !fileInfo2.getIsLeaf()) {
                    return 1;
                } else {
                    // 如果isLeaf属性相同，则按照其他属性排序，这里可以根据具体情况进行修改
                    // 例如，按照文件标题排序
                    return fileInfo1.getTitle().compareTo(fileInfo2.getTitle());
                }
            }
        });
    }

    /**
     * 根据路径查询所有文件
     *
     * @param path 路径
     * @return 树形目录
     */
    public List<FileInfoVo> listFiles(String path, Boolean onlyFolder) {
        try {
            List<FileInfoVo> fileList = new ArrayList<>();
            if (path == null || path.isEmpty()) {
                // 如果路径为空，则根据不同操作系统获取根目录路径
                return getRootDirectoryPath();
            }
            File directory = new File(path);
            if (!directory.exists() || !directory.isDirectory()) {
                // 如果路径不存在或者不是文件夹，返回空列表
                return fileList;
            }
            // 获取目录下的所有文件和子文件夹
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    boolean isImage = isImage(file);
                    FileInfoVo fileInfo = new FileInfoVo();
                    fileInfo.setTitle(file.getName());
                    fileInfo.setKey(file.getPath());
                    if (file.isDirectory()) {
                        fileInfo.setType("folder");
                        fileInfo.setIsLeaf(false);
                        fileList.add(fileInfo);
                    } else if (!onlyFolder) {
                        fileInfo.setIsLeaf(true);
                        if (isImage) {
                            fileInfo.setSrc(convertImageToBase64(file.getPath()));
                            fileInfo.setType("image");
                        }

                        fileList.add(fileInfo);
                    }
                }
            }
            sortFileInfoList(fileList);
            return fileList;
        } catch (Error e) {

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }


    /**
     * 根据路径删除文件
     *
     * @param paths 路径
     * @return 是否成功
     */
    @Override
    public boolean deleteFile(String paths) {
        if (StringUtils.isEmpty(paths)) {
            return false;
        }

        // 按逗号分隔路径
        String[] pathArray = paths.split(",");
        boolean allDeleted = true;

        for (String path : pathArray) {
            try {
                boolean isDelete = FileUtil.del(new File(path));
                if (isDelete) {
                    allDeleted = true;
                }

            } catch (Exception e) {
                allDeleted = false;
            }
        }
        return allDeleted;
    }

    @SneakyThrows
    @Override
    public ResponseEntity<byte[]> previewFile(String path) {
        return viewFile(path);
    }

    /**
     * 浏览文件，暂时未调用，用于浏览媒体专用
     *
     * @param path 路径
     * @return 是否成功
     */
    public ResponseEntity<byte[]> viewFile(String path) throws IOException {
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.toString());
        // 根据文件路径创建文件对象
        File file = new File(decodedPath);

        // 检查文件是否存在
        if (!file.exists() || !file.isFile()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        // 设置响应头，告诉客户端文件类型
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", file.getName());
        // 获取文件输入流
        InputStream inputStream = Files.newInputStream(file.toPath());

        // 返回文件流作为响应体
        return new ResponseEntity<>(FileCopyUtils.copyToByteArray(inputStream), headers, HttpStatus.OK);

    }

    /**
     * 根据路径创建文件
     *
     * @param path 路径
     * @param type file:文件，folder：文件夹
     * @return 是否成功
     */
    @Override
    public boolean createFile(String path, String type) {
        File file = new File(path);

        if ("file".equalsIgnoreCase(type)) {
            try {
                // 尝试创建文件
                return file.createNewFile();
            } catch (Exception e) {
                return false;
            }
        } else if ("folder".equalsIgnoreCase(type)) {
            // 尝试创建文件夹
            return file.mkdirs();
        } else {
            return false;
        }
    }

    /**
     * 根据路径重命名文件或文件夹
     *
     * @param oldPath 旧的路径
     * @param newPath 新的路径
     * @return 是否成功
     */
    @Override
    public boolean renameFile(String oldPath, String newPath) {
        File oldFile = new File(oldPath);
        File newFile = new File(newPath);
        return oldFile.exists() && oldFile.renameTo(newFile);
    }

    /**
     * 复制文件
     *
     * @param targetPath 目标路径
     * @param files      复制的文件列表
     * @return 是否成功
     */
    @Override
    public boolean cloneFiles(String targetPath, String files) {

        if (!FileUtil.isDirectory(targetPath)) {
            return false;
        }

        String[] fileArray = files.split(",");
        for (String filePath : fileArray) {
            File sourceFile = new File(filePath.trim());
            if (!sourceFile.isFile() || !sourceFile.exists()) {
                return false;
            }

            File targetFile = new File(targetPath, sourceFile.getName());

            try {
                FileUtil.copy(sourceFile, targetFile, true);
            } catch (Exception e) {
                return false;
            }
        }

        return true;
    }


    /**
     * 查询文件信息
     *
     * @param path 文件的路径
     */
    @Override
    public FileAttr fileInfo(String path) {
        File file = new File(path);

        // 检查文件是否存在
        if (!file.exists()) {
            return null;
        }

        // 创建一个FileInfo对象，用于存储文件信息
        FileAttr fileInfo = new FileAttr();

        // 设置文件名
        fileInfo.setName(file.getName());

        // 设置文件路径
        fileInfo.setAbsolutePath(file.getAbsolutePath());

        // 设置文件大小（以字节为单位）
        fileInfo.setSize(fileInfo.formatFileSize(file.length()));

        // 设置文件是否可读
        fileInfo.setReadable(file.canRead());

        // 设置文件是否可写
        fileInfo.setWritable(file.canWrite());

        // 设置文件是否是目录
        fileInfo.setDirectory(file.isDirectory());

        // 设置文件是否是隐藏文件
        fileInfo.setHidden(file.isHidden());

        // 设置文件最后修改时间
        fileInfo.setLastModified(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file.lastModified()));

        // 设置文件创建时间
        fileInfo.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file.lastModified()));

        return fileInfo;
    }

    /**
     * 保存文件
     *
     * @param path    文件路径
     * @param content 文件内容
     * @return 结果
     */
    @Override
    public boolean saveFile(String path, String content) {
        FileUtil.writeUtf8String(content, path);
        return true;
    }

    /**
     * @param path
     * @return
     */
    @Override
    public ResponseEntity<byte[]> download(String path) {
        File file = new File(path);
        if (!file.exists() || !file.isFile()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDispositionFormData("attachment", file.getName());

            byte[] fileContent = Files.readAllBytes(file.toPath());

            return new ResponseEntity<>(fileContent, headers, HttpStatus.OK);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


    /**
     * 获取系统根目录，根据不同操作系统
     */
    public static List<FileInfoVo> getRootDirectoryPath() {
        List<FileInfoVo> rootPaths = new ArrayList<>();
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            File[] roots = File.listRoots();
            for (File root : roots) {
                rootPaths.add(createFileInfo(root.getPath()));
            }
        } else {
            File rootDirectory = new File("/");
            if (rootDirectory.exists() && rootDirectory.isDirectory()) {
                File[] directories = rootDirectory.listFiles(File::isDirectory);
                if (directories != null) {
                    for (File directory : directories) {
                        rootPaths.add(createFileInfo(directory.getAbsolutePath()));
                    }
                }
            }
        }
        return rootPaths;
    }

    /**
     * @param path 路径
     * @return fileInfo 文件相关信息
     */
    private static FileInfoVo createFileInfo(String path) {
        FileInfoVo fileInfo = new FileInfoVo();
        fileInfo.setTitle(path);
        fileInfo.setType("folder");
        fileInfo.setKey(path);
        fileInfo.setIsLeaf(false);
        return fileInfo;
    }
}
