/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : 150.158.14.110:3306
 Source Schema         : ax_test

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 12/08/2024 16:45:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ax_district
-- ----------------------------
DROP TABLE IF EXISTS `ax_district`;
CREATE TABLE `ax_district`  (
  `district_id` smallint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `pid` smallint UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级关系',
  `district` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地区名称',
  `level` tinyint(1) NOT NULL COMMENT '子属关系',
  PRIMARY KEY (`district_id`) USING BTREE,
  INDEX `parent_id`(`pid`) USING BTREE,
  INDEX `region_type`(`level`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3434 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '地区表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ax_district
-- ----------------------------

-- ----------------------------
-- Table structure for ax_files
-- ----------------------------
DROP TABLE IF EXISTS `ax_files`;
CREATE TABLE `ax_files`  (
  `file_id` int NOT NULL AUTO_INCREMENT COMMENT '文件唯一标识',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件名称',
  `file_path` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件存储路径',
  `file_size` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件大小（字节）',
  `file_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件类型',
  `file_description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文件描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录最后更新时间',
  `user_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `status` smallint NULL DEFAULT NULL COMMENT '审核状态',
  `file_storage_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `external_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文件管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ax_files
-- ----------------------------

-- ----------------------------
-- Table structure for ax_memo
-- ----------------------------
DROP TABLE IF EXISTS `ax_memo`;
CREATE TABLE `ax_memo`  (
  `memo_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备忘录标题',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备忘录的值',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备忘录描述',
  `extra` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '附加值',
  `user_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '1：备忘录，2：记事本',
  `dept_id` int NULL DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`memo_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 153 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ax_memo
-- ----------------------------

-- ----------------------------
-- Table structure for ax_test
-- ----------------------------
DROP TABLE IF EXISTS `ax_test`;
CREATE TABLE `ax_test`  (
  `test_id` int NOT NULL AUTO_INCREMENT COMMENT '测试ID',
  `district` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地区选择',
  `image_field` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片选择',
  `md_field` datetime NULL DEFAULT NULL COMMENT 'Markdown文件',
  `card_cover` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '卡片封面',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `model` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '三向绑定',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`test_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 269 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ax_test
-- ----------------------------

-- ----------------------------
-- Table structure for ax_user_assets
-- ----------------------------
DROP TABLE IF EXISTS `ax_user_assets`;
CREATE TABLE `ax_user_assets`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `menu_id` int NULL DEFAULT NULL,
  `theme_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户资源表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ax_user_assets
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `field_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段标题，标注字段',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `table_column_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表列名',
  `field_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `is_title` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标记卡片的标题',
  `is_cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标记卡片的封面',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 668 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin', '2024-04-10 15:08:50', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2024-04-10 15:08:50', 'admin', '2024-04-13 00:19:45', '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (6, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2024-04-10 15:08:50', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');
INSERT INTO `sys_config` VALUES (117, '系统配置', 'sys.settings', 'eyJwYWdlVGl0bGUiOiJBWOWQjuWPsOeuoeeQhuezu+e7nyIsInBhZ2VJY29uIjoiIiwidmVyc2lvbiI6IjEuMS4xIiwic3RhdGljUmVzb3VyY2VVcmwiOiJodHRwOi8vMTUwLjE1OC4xNC4xMTA6ODAwMi9pbWFnZXMvIiwid2VsY29tZUxpbmsiOiIiLCJwYWdlQ29uZmlnIjp7InBhZ2VUaXRsZSI6IkFY5ZCO56uv566h55CG57O757ufIiwicGFnZUljb24iOiIiLCJ2ZXJzaW9uIjoiMS4xLjMiLCJzdGF0aWNSZXNvdXJjZVVybCI6Imh0dHBzOi8vaW4tZ2l0LmdpdGh1Yi5pby9heC1yZXNvdXJjZS9pbWFnZXMvIiwid2VsY29tZUxpbmsiOiJodHRwczovL3d3dy55dXF1ZS5jb20vdTMyOTk3ODk5L2F4L2t2MnNxNWkwbnJ6MzgzMWMifSwiZ2VuQ29uZmlnIjp7InBhcmVudElkIjowLCJtb2R1bGVOYW1lIjoiIn19', 'N', 'admin', '2024-07-30 09:45:32', 'admin', '2024-08-12 11:04:40', '系统配置');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', 'AX', 0, 'ax', '176666666', 'ax@qq.com', '0', '0', 'admin', '2024-04-10 15:08:49', 'admin', '2024-04-14 21:09:36');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '测试', 1, 'AX', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-04-10 15:08:49', 'admin', '2024-05-16 22:50:08');
INSERT INTO `sys_dept` VALUES (200, 100, '0,100', '访客', 0, 'AX', '1333333333', NULL, '0', '0', 'admin', '2024-04-10 20:32:22', 'admin', '2024-05-16 22:50:28');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '字典键值',
  `dict_type` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '字典类型',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `addition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典的额外描述信息，用于描述该字典的其他作用',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '性别男', NULL);
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '性别女', NULL);
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '性别未知', NULL);
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '显示菜单', NULL);
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '隐藏菜单', NULL);
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '正常状态', NULL);
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '停用状态', NULL);
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '正常状态', NULL);
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '停用状态', NULL);
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '默认分组', NULL);
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '系统分组', NULL);
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '系统默认是', NULL);
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '系统默认否', NULL);
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '通知', NULL);
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '公告', NULL);
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', 'Y', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '正常状态', NULL);
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '关闭状态', NULL);
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '其他操作', NULL);
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '新增操作', NULL);
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '修改操作', NULL);
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '删除操作', NULL);
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '授权操作', NULL);
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '导出操作', NULL);
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '导入操作', NULL);
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '强退操作', NULL);
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '生成操作', NULL);
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '清空操作', NULL);
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '正常状态', NULL);
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', 'N', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '停用状态', NULL);
INSERT INTO `sys_dict_data` VALUES (147, 6, '壁纸', 'wallpaper', 'sys_gallery', 'N', '0', 'admin', '2024-07-25 15:41:00', 'admin', '2024-07-25 16:11:30', '壁纸', NULL);
INSERT INTO `sys_dict_data` VALUES (148, 3, '头像', 'avatar', 'sys_gallery', 'N', '0', 'admin', '2024-07-25 15:41:38', 'admin', '2024-07-25 16:11:14', '头像', NULL);
INSERT INTO `sys_dict_data` VALUES (150, 0, '默认图标', 'image-icon', 'sys_gallery', 'N', '0', 'admin', '2024-07-25 15:43:15', 'admin', '2024-07-25 16:11:37', '自定义图标', NULL);
INSERT INTO `sys_dict_data` VALUES (151, 0, '自定义', 'custom', 'sys_gallery', 'N', '0', 'admin', '2024-07-25 18:39:46', '', NULL, '自定义风格', NULL);
INSERT INTO `sys_dict_data` VALUES (152, 0, '系统图标', 'system', 'sys_gallery', 'N', '0', 'admin', '2024-07-25 22:48:28', '', NULL, '内置图标', NULL);
INSERT INTO `sys_dict_data` VALUES (153, 1, '图片', 'IMAGE', 'sys_file_type', 'N', '0', 'admin', '2024-07-26 15:06:50', 'admin', '2024-07-28 23:31:14', '图片类型', NULL);
INSERT INTO `sys_dict_data` VALUES (154, 4, '视频', 'VIDEO', 'sys_file_type', 'N', '0', 'admin', '2024-07-26 15:07:27', 'admin', '2024-07-28 23:31:07', '视频', NULL);
INSERT INTO `sys_dict_data` VALUES (155, 6, '文档', 'DOC', 'sys_file_type', 'N', '0', 'admin', '2024-07-26 15:07:44', 'admin', '2024-07-28 23:31:51', '文档类型', NULL);
INSERT INTO `sys_dict_data` VALUES (156, 1, '音频', 'AUDIO', 'sys_file_type', 'N', '0', 'admin', '2024-07-26 15:09:58', 'admin', '2024-07-28 23:31:39', '音频', NULL);
INSERT INTO `sys_dict_data` VALUES (157, 5, '其他', 'OTHER', 'sys_file_type', 'N', '0', 'admin', '2024-07-26 15:10:15', 'admin', '2024-07-28 23:31:48', '其他', NULL);
INSERT INTO `sys_dict_data` VALUES (162, 0, '全部', '', 'sys_file_type', 'N', '0', 'admin', '2024-07-28 23:31:01', '', NULL, '任意类型', NULL);
INSERT INTO `sys_dict_data` VALUES (166, 0, 'axTest', '{\"dictType\":\"sys_form_message\",\"moduleName\":\"测试\",\"columns\":[{\"title\":\"测试ID\",\"message\":\"PHA+5rWL6K+VSUQ8L3A+\",\"key\":\"testId\"},{\"title\":\"地区选择\",\"message\":\"PHA+5Zyw5Yy6PC9wPg==\",\"key\":\"district\"},{\"title\":\"图片选择\",\"message\":\"PHA+5Zu+54mHPC9wPg==\",\"key\":\"imageField\"},{\"title\":\"Markdown文件\",\"message\":\"PHA+TUQ8L3A+PHA+PGJyPjwvcD4=\",\"key\":\"mdField\"},{\"title\":\"卡片封面\",\"message\":\"PHA+5Y2h54mHPC9wPg==\",\"key\":\"cardCover\"},{\"title\":\"备注信息\",\"message\":\"PHA+5aSH5rOoPC9wPg==\",\"key\":\"remark\"},{\"title\":\"三向绑定\",\"message\":\"PHA+5LiJ5ZCRPC9wPg==\",\"key\":\"model\"},{\"title\":\"\",\"message\":\"\",\"key\":\"createTime\"},{\"title\":\"\",\"message\":\"\",\"key\":\"updateTime\"}],\"rules\":{}}', 'sys_form_message', 'N', '0', 'admin', '2024-08-09 11:25:22', 'admin', '2024-08-09 11:25:53', NULL, '{\"title\":\"测试\",\"desc\":\"测试表\"}');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2024-04-10 15:08:50', 'admin', '2024-05-13 12:22:49', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2024-04-10 15:08:50', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (104, '表单提示', 'sys_form_message', '0', 'admin', '2024-07-10 16:53:46', 'admin', '2024-07-11 00:37:01', '用于描述表单提示信息');
INSERT INTO `sys_dict_type` VALUES (105, '系统图库', 'sys_gallery', '0', 'admin', '2024-07-25 15:39:38', 'admin', '2024-07-25 15:45:05', '系统图库,用于读取系统图标');
INSERT INTO `sys_dict_type` VALUES (106, '文件类型', 'sys_file_type', '0', 'admin', '2024-07-26 15:05:53', '', NULL, '文件类型');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2024-04-10 15:08:50', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2024-04-10 15:08:50', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2024-04-10 15:08:50', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status` ASC) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1815 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '路由参数',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `height` int NULL DEFAULT NULL COMMENT '窗口高度',
  `width` int NULL DEFAULT NULL COMMENT '窗口宽度',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2233 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 2, 'system', NULL, '', 0, 'M', '0', '0', '', '', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-12 15:58:19', '1', NULL, NULL);
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 0, 'C', '0', '0', 'system:user:list', 'system/user.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:51:46', '用户管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (101, '身份管理', 1, 0, 'role', 'system/role/index', '', 0, 'C', '0', '0', 'system:role:list', 'system/identity.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:53:15', '角色管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 0, 'C', '0', '0', 'system:menu:list', 'system/menu.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:51:11', '菜单管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 0, 'C', '0', '0', 'system:dept:list', 'system/dept.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:52:07', '部门管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 0, 'C', '0', '0', 'system:post:list', 'system/post.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:52:19', '岗位管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 0, 'C', '0', '0', 'system:dict:list', 'system/dict.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:54:36', '字典管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 0, 'C', '0', '0', 'system:config:list', 'system/config.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-25 23:55:06', '参数设置菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 0, 'C', '0', '0', 'system:notice:list', 'system/notice.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-26 14:02:11', '通知公告菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 0, 'M', '0', '0', '', '', 'admin', '2024-04-10 15:08:49', '', NULL, '日志管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (109, '在线用户', 1, 1, 'online', 'monitor/online/index', '', 0, 'C', '0', '0', 'monitor:online:list', 'system/online.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-09 12:58:09', '在线用户菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (110, '定时任务', 1, 2, 'job', 'monitor/job/index', '', 0, 'C', '0', '0', 'monitor:job:list', 'system/schedule.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-09 12:58:18', '定时任务菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (111, '数据监控', 1, 3, 'druid', 'monitor/druid/index', '', 0, 'C', '0', '0', 'monitor:druid:list', 'system/data.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-09 12:58:28', '数据监控菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (112, '服务监控', 1, 4, 'server', 'monitor/server/index', '', 0, 'C', '0', '0', 'monitor:server:list', 'system/monitoring.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-09 12:58:45', '服务监控菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 1, 6, 'cache', 'monitor/cache/index', '', 0, 'C', '0', '0', 'monitor:cache:list', 'system/cache.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-09 12:58:37', '缓存列表菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (116, '代码工具', 2218, 2, 'gen', 'tool/gen/index', '', 0, 'C', '0', '0', 'tool:gen:list', 'system/gen.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-07 14:49:00', '代码生成菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 0, 'C', '0', '0', 'monitor:operlog:list', 'system/operation.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-26 00:06:22', '操作日志菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 0, 'C', '0', '0', 'monitor:logininfor:list', 'system/monitoring.png', 'admin', '2024-04-10 15:08:49', 'admin', '2024-07-26 00:06:52', '登录日志菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 0, 'F', '0', '0', 'system:user:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 0, 'F', '0', '0', 'system:user:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 0, 'F', '0', '0', 'system:user:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 0, 'F', '0', '0', 'system:user:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 0, 'F', '0', '0', 'system:user:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 0, 'F', '0', '0', 'system:user:import', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 0, 'F', '0', '0', 'system:user:resetPwd', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1007, '身份查询', 101, 1, '', '', '', 0, 'F', '0', '0', 'system:role:query', NULL, 'admin', '2024-04-10 15:08:49', 'admin', '2024-04-10 20:29:52', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1008, '身份新增', 101, 2, '', '', '', 0, 'F', '0', '0', 'system:role:add', NULL, 'admin', '2024-04-10 15:08:49', 'admin', '2024-04-10 20:30:01', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1009, '身份修改', 101, 3, '', '', '', 0, 'F', '0', '0', 'system:role:edit', NULL, 'admin', '2024-04-10 15:08:49', 'admin', '2024-04-10 20:30:05', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1010, '身份删除', 101, 4, '', '', '', 0, 'F', '0', '0', 'system:role:remove', NULL, 'admin', '2024-04-10 15:08:49', 'admin', '2024-04-10 20:30:09', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1011, '身份导出', 101, 5, '', '', '', 0, 'F', '0', '0', 'system:role:export', NULL, 'admin', '2024-04-10 15:08:49', 'admin', '2024-04-10 20:30:14', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 0, 'F', '0', '0', 'system:menu:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 0, 'F', '0', '0', 'system:menu:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 0, 'F', '0', '0', 'system:menu:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 0, 'F', '0', '0', 'system:menu:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 0, 'F', '0', '0', 'system:dept:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 0, 'F', '0', '0', 'system:dept:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 0, 'F', '0', '0', 'system:dept:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 0, 'F', '0', '0', 'system:dept:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 0, 'F', '0', '0', 'system:post:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 0, 'F', '0', '0', 'system:post:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 0, 'F', '0', '0', 'system:post:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 0, 'F', '0', '0', 'system:post:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 0, 'F', '0', '0', 'system:post:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 0, 'F', '0', '0', 'system:dict:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 0, 'F', '0', '0', 'system:dict:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 0, 'F', '0', '0', 'system:dict:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 0, 'F', '0', '0', 'system:dict:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 0, 'F', '0', '0', 'system:dict:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 0, 'F', '0', '0', 'system:config:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 0, 'F', '0', '0', 'system:config:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 0, 'F', '0', '0', 'system:config:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 0, 'F', '0', '0', 'system:config:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 0, 'F', '0', '0', 'system:config:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 0, 'F', '0', '0', 'system:notice:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 0, 'F', '0', '0', 'system:notice:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 0, 'F', '0', '0', 'system:notice:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 0, 'F', '0', '0', 'system:notice:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 0, 'F', '0', '0', 'monitor:operlog:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 0, 'F', '0', '0', 'monitor:operlog:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 0, 'F', '0', '0', 'monitor:operlog:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 0, 'F', '0', '0', 'monitor:logininfor:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 0, 'F', '0', '0', 'monitor:logininfor:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 0, 'F', '0', '0', 'monitor:logininfor:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 0, 'F', '0', '0', 'monitor:logininfor:unlock', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 0, 'F', '0', '0', 'monitor:online:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 0, 'F', '0', '0', 'monitor:online:batchLogout', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 0, 'F', '0', '0', 'monitor:online:forceLogout', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 0, 'F', '0', '0', 'monitor:job:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 0, 'F', '0', '0', 'monitor:job:add', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 0, 'F', '0', '0', 'monitor:job:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 0, 'F', '0', '0', 'monitor:job:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 0, 'F', '0', '0', 'monitor:job:changeStatus', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 0, 'F', '0', '0', 'monitor:job:export', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 0, 'F', '0', '0', 'tool:gen:query', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 0, 'F', '0', '0', 'tool:gen:edit', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 0, 'F', '0', '0', 'tool:gen:remove', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 0, 'F', '0', '0', 'tool:gen:import', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 0, 'F', '0', '0', 'tool:gen:preview', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 0, 'F', '0', '0', 'tool:gen:code', NULL, 'admin', '2024-04-10 15:08:49', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2100, '系统数据', 0, 1, 'tool', NULL, NULL, 1, 'M', '0', '0', '', 'system/sys-config.png', 'admin', '2024-05-07 10:00:24', 'admin', '2024-08-12 15:58:24', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2101, '文件管理', 2100, 0, 'service-folder', 'system/folder/index', NULL, 1, 'C', '0', '0', '', 'system/folder.png', 'admin', '2024-05-09 23:14:13', 'admin', '2024-07-04 13:52:40', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2102, '新建', 2101, 0, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:add', '#', 'admin', '2024-05-09 23:15:14', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2103, '删除', 2101, 1, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:delete', '#', 'admin', '2024-05-09 23:15:39', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2104, '重命名', 2101, 0, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:rename', '#', 'admin', '2024-05-09 23:15:52', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2105, '查看', 2101, 0, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:view', '#', 'admin', '2024-05-09 23:16:08', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2106, '分享', 2101, 5, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:share', '#', 'admin', '2024-05-09 23:16:26', 'admin', '2024-05-11 17:27:26', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2107, '复制', 2101, 7, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:clone', '#', 'admin', '2024-05-10 19:27:23', 'admin', '2024-05-11 17:27:15', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2108, '上传', 2101, 6, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:upload', '#', 'admin', '2024-05-11 17:26:32', 'admin', '2024-05-11 17:27:21', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2109, '属性', 2101, 8, '', NULL, NULL, 1, 'F', '0', '0', 'tool:file:attr', '#', 'admin', '2024-05-11 17:26:48', 'admin', '2024-05-11 17:27:09', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2110, '下载', 2101, 6, '', NULL, NULL, 1, 'F', '1', '0', 'tool:file:download', '#', 'admin', '2024-05-22 12:36:22', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2164, '表单配置', 2218, 1, 'form-config', 'tool/form-config/index', NULL, 1, 'C', '0', '0', '', 'system/dict.png', 'admin', '2024-07-11 14:02:03', 'admin', '2024-08-08 14:56:14', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2166, '系统介绍', 0, 5, 'https://www.yuque.com/u32997899/sluwug/kv2sq5i0nrz3831c', NULL, NULL, 1, 'L', '0', '0', NULL, '#', 'admin', '2024-07-13 13:59:22', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2180, '用户文件', 2232, 1, 'files', 'ax/files/index', NULL, 0, 'C', '0', '0', 'ax:files:list', 'system/file.png', 'admin', '2024-07-26 15:23:29', 'admin', '2024-08-09 17:39:12', '文件管理菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2181, '文件管理查询', 2180, 1, '#', '', NULL, 0, 'F', '0', '0', 'ax:files:query', '#', 'admin', '2024-07-26 15:23:29', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2182, '文件管理新增', 2180, 2, '#', '', NULL, 0, 'F', '0', '0', 'ax:files:add', '#', 'admin', '2024-07-26 15:23:29', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2183, '文件管理修改', 2180, 3, '#', '', NULL, 0, 'F', '0', '0', 'ax:files:edit', '#', 'admin', '2024-07-26 15:23:29', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2184, '文件管理删除', 2180, 4, '#', '', NULL, 0, 'F', '0', '0', 'ax:files:remove', '#', 'admin', '2024-07-26 15:23:29', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2185, '文件管理导出', 2180, 5, '#', '', NULL, 0, 'F', '0', '0', 'ax:files:export', '#', 'admin', '2024-07-26 15:23:29', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2190, '测试表', 2100, 1, 'test', 'ax/test/index', NULL, 0, 'C', '0', '0', 'ax:test:list', 'system/test.png', 'admin', '2024-08-02 09:21:25', 'admin', '2024-08-02 09:48:27', '测试表菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2191, '测试表查询', 2190, 1, '#', '', NULL, 0, 'F', '0', '0', 'ax:test:query', '#', 'admin', '2024-08-02 09:21:25', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2192, '测试表新增', 2190, 2, '#', '', NULL, 0, 'F', '0', '0', 'ax:test:add', '#', 'admin', '2024-08-02 09:21:25', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2193, '测试表修改', 2190, 3, '#', '', NULL, 0, 'F', '0', '0', 'ax:test:edit', '#', 'admin', '2024-08-02 09:21:25', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2194, '测试表删除', 2190, 4, '#', '', NULL, 0, 'F', '0', '0', 'ax:test:remove', '#', 'admin', '2024-08-02 09:21:26', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2195, '测试表导出', 2190, 5, '#', '', NULL, 0, 'F', '0', '0', 'ax:test:export', '#', 'admin', '2024-08-02 09:21:26', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2196, '备忘录', 2232, 1, 'memo', 'ax/memo/index', NULL, 0, 'C', '0', '0', 'ax:memo:list', 'system/notepad.png', 'admin', '2024-08-02 12:07:36', 'admin', '2024-08-09 17:39:21', '备忘录菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2197, '备忘录查询', 2196, 1, '', '', NULL, 0, 'F', '0', '0', 'ax:memo:query', '', 'admin', '2024-08-02 12:07:36', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2198, '备忘录新增', 2196, 2, '', '', NULL, 0, 'F', '0', '0', 'ax:memo:add', '', 'admin', '2024-08-02 12:07:36', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2199, '备忘录修改', 2196, 3, '', '', NULL, 0, 'F', '0', '0', 'ax:memo:edit', '', 'admin', '2024-08-02 12:07:36', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2200, '备忘录删除', 2196, 4, '', '', NULL, 0, 'F', '0', '0', 'ax:memo:remove', '', 'admin', '2024-08-02 12:07:36', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2201, '备忘录导出', 2196, 5, '', '', NULL, 0, 'F', '0', '0', 'ax:memo:export', '', 'admin', '2024-08-02 12:07:36', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2211, '系统配置', 1, 0, '', 'tool/system-setting/index', NULL, 1, 'C', '0', '0', '', 'system/sys-config.png', 'admin', '2024-08-03 00:48:12', 'admin', '2024-08-09 13:01:09', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2212, '地区', 2100, 1, 'district', 'ax/district/index', NULL, 0, 'C', '0', '0', 'ax:district:list', '', 'admin', '2024-08-04 16:55:52', '', NULL, '地区菜单', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2213, '地区查询', 2212, 1, '', '', NULL, 0, 'F', '0', '0', 'ax:district:query', '', 'admin', '2024-08-04 16:55:53', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2214, '地区新增', 2212, 2, '', '', NULL, 0, 'F', '0', '0', 'ax:district:add', '', 'admin', '2024-08-04 16:55:53', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2215, '地区修改', 2212, 3, '', '', NULL, 0, 'F', '0', '0', 'ax:district:edit', '', 'admin', '2024-08-04 16:55:53', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2216, '地区删除', 2212, 4, '', '', NULL, 0, 'F', '0', '0', 'ax:district:remove', '', 'admin', '2024-08-04 16:55:53', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2217, '地区导出', 2212, 5, '', '', NULL, 0, 'F', '0', '0', 'ax:district:export', '', 'admin', '2024-08-04 16:55:53', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2218, '单表生成', 0, 4, '', NULL, NULL, 1, 'M', '0', '0', '', NULL, 'admin', '2024-08-07 14:48:44', 'admin', '2024-08-08 14:56:43', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2219, '单页生成', 2218, 0, '', 'tool/sfc/index', NULL, 1, 'C', '0', '0', '', 'image-icon/blank-file.png', 'admin', '2024-08-07 14:51:39', 'admin', '2024-08-08 18:21:49', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2232, '我的数据', 0, 0, '', NULL, NULL, 1, 'M', '0', '0', NULL, NULL, 'admin', '2024-08-09 17:38:31', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type` ASC) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status` ASC) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 330 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2024-04-10 15:08:49', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2024-04-10 15:08:49', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2024-04-10 15:08:49', 'admin', '2024-05-20 19:10:45', '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2024-04-10 15:08:49', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2024-04-10 15:08:49', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '观察员', 'observer', 2, '1', 1, 1, '0', '0', 'admin', '2024-04-10 15:08:49', 'admin', '2024-08-12 16:20:52', '能看到整个系统完整功能的身份');
INSERT INTO `sys_role` VALUES (100, '访客', 'visitor', 3, '5', 1, 0, '0', '0', 'admin', '2024-04-10 20:31:02', 'admin', '2024-08-02 09:25:38', '访客，只能访问公开的数据');
INSERT INTO `sys_role` VALUES (101, '测试', 'test', 0, '2', 1, 1, '0', '0', 'admin', '2024-04-28 16:18:20', 'admin', '2024-05-20 19:15:32', '测试信息');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 105);
INSERT INTO `sys_role_dept` VALUES (100, 200);
INSERT INTO `sys_role_dept` VALUES (101, 100);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 2100);
INSERT INTO `sys_role_menu` VALUES (2, 2101);
INSERT INTO `sys_role_menu` VALUES (2, 2104);
INSERT INTO `sys_role_menu` VALUES (2, 2105);
INSERT INTO `sys_role_menu` VALUES (2, 2106);
INSERT INTO `sys_role_menu` VALUES (2, 2109);
INSERT INTO `sys_role_menu` VALUES (2, 2110);
INSERT INTO `sys_role_menu` VALUES (2, 2164);
INSERT INTO `sys_role_menu` VALUES (2, 2166);
INSERT INTO `sys_role_menu` VALUES (2, 2180);
INSERT INTO `sys_role_menu` VALUES (2, 2181);
INSERT INTO `sys_role_menu` VALUES (2, 2182);
INSERT INTO `sys_role_menu` VALUES (2, 2183);
INSERT INTO `sys_role_menu` VALUES (2, 2184);
INSERT INTO `sys_role_menu` VALUES (2, 2185);
INSERT INTO `sys_role_menu` VALUES (2, 2190);
INSERT INTO `sys_role_menu` VALUES (2, 2191);
INSERT INTO `sys_role_menu` VALUES (2, 2192);
INSERT INTO `sys_role_menu` VALUES (2, 2193);
INSERT INTO `sys_role_menu` VALUES (2, 2194);
INSERT INTO `sys_role_menu` VALUES (2, 2195);
INSERT INTO `sys_role_menu` VALUES (2, 2196);
INSERT INTO `sys_role_menu` VALUES (2, 2197);
INSERT INTO `sys_role_menu` VALUES (2, 2198);
INSERT INTO `sys_role_menu` VALUES (2, 2199);
INSERT INTO `sys_role_menu` VALUES (2, 2200);
INSERT INTO `sys_role_menu` VALUES (2, 2201);
INSERT INTO `sys_role_menu` VALUES (2, 2212);
INSERT INTO `sys_role_menu` VALUES (2, 2213);
INSERT INTO `sys_role_menu` VALUES (2, 2214);
INSERT INTO `sys_role_menu` VALUES (2, 2215);
INSERT INTO `sys_role_menu` VALUES (2, 2216);
INSERT INTO `sys_role_menu` VALUES (2, 2217);
INSERT INTO `sys_role_menu` VALUES (2, 2218);
INSERT INTO `sys_role_menu` VALUES (2, 2219);
INSERT INTO `sys_role_menu` VALUES (2, 2232);
INSERT INTO `sys_role_menu` VALUES (100, 2100);
INSERT INTO `sys_role_menu` VALUES (100, 2166);
INSERT INTO `sys_role_menu` VALUES (100, 2180);
INSERT INTO `sys_role_menu` VALUES (100, 2181);
INSERT INTO `sys_role_menu` VALUES (100, 2182);
INSERT INTO `sys_role_menu` VALUES (100, 2183);
INSERT INTO `sys_role_menu` VALUES (100, 2184);
INSERT INTO `sys_role_menu` VALUES (100, 2185);
INSERT INTO `sys_role_menu` VALUES (100, 2190);
INSERT INTO `sys_role_menu` VALUES (100, 2191);
INSERT INTO `sys_role_menu` VALUES (100, 2192);
INSERT INTO `sys_role_menu` VALUES (100, 2193);
INSERT INTO `sys_role_menu` VALUES (100, 2194);
INSERT INTO `sys_role_menu` VALUES (100, 2195);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 122 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', 'AX超管', '00', 'ax@qq.com', '15888888888', '1', 'avatar/3DDD-1.png', '$2a$10$U/EC.RkbXkxXcLh805o2iubzS0Wbf51m/E.sfWANODdt3OBzRP32G', '0', '0', '127.0.0.1', '2024-08-12 16:20:20', 'admin', '2024-04-10 15:08:49', '', '2024-08-12 16:20:20', '管理员');
INSERT INTO `sys_user` VALUES (112, 100, 'observer', '观察员', '00', 'observer@qq.com', '17666666666', '0', 'avatar/OSLO-3.png', '$2a$10$UtS5NtjZkVKpc7M..VaDC.ZOzdCRDXj51HdtAIRPqvbZVnJZnX4bq', '0', '0', '127.0.0.1', '2024-08-12 16:21:03', 'admin', '2024-04-21 18:08:19', 'admin', '2024-08-12 16:21:02', '能看到完整的系统信息');
INSERT INTO `sys_user` VALUES (113, 101, 'test02', '测试账号', '00', 'test@qq.com', '12311111111', '0', '3DDD.png', '$2a$10$K2Gm1xsPdJ4qSOfH22PTDOm.wGgAgd0T4HTM5D9qflkDrEolo3Rga', '0', '0', '127.0.0.1', '2024-08-12 16:11:48', '', '2024-04-22 19:40:59', 'admin', '2024-08-12 16:11:47', NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
INSERT INTO `sys_user_post` VALUES (112, 4);
INSERT INTO `sys_user_post` VALUES (114, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 100);
INSERT INTO `sys_user_role` VALUES (101, 100);
INSERT INTO `sys_user_role` VALUES (102, 100);
INSERT INTO `sys_user_role` VALUES (104, 100);
INSERT INTO `sys_user_role` VALUES (105, 100);
INSERT INTO `sys_user_role` VALUES (106, 100);
INSERT INTO `sys_user_role` VALUES (107, 100);
INSERT INTO `sys_user_role` VALUES (108, 100);
INSERT INTO `sys_user_role` VALUES (109, 100);
INSERT INTO `sys_user_role` VALUES (110, 100);
INSERT INTO `sys_user_role` VALUES (111, 100);
INSERT INTO `sys_user_role` VALUES (112, 2);
INSERT INTO `sys_user_role` VALUES (113, 100);
INSERT INTO `sys_user_role` VALUES (114, 101);
INSERT INTO `sys_user_role` VALUES (115, 100);
INSERT INTO `sys_user_role` VALUES (116, 100);
INSERT INTO `sys_user_role` VALUES (117, 100);
INSERT INTO `sys_user_role` VALUES (118, 100);
INSERT INTO `sys_user_role` VALUES (119, 100);
INSERT INTO `sys_user_role` VALUES (120, 100);
INSERT INTO `sys_user_role` VALUES (121, 100);

SET FOREIGN_KEY_CHECKS = 1;
